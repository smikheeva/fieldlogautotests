package mrs;

import com.plotpad.service.projects.api.dto.ProjectInDto;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.restassured.http.ContentType;
import mrs.Pages.PageLogin;
import mrs.Pages.PageObject;
import org.apache.commons.lang3.RandomStringUtils;
import org.awaitility.Duration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.html5.WebStorage;
import org.openqa.selenium.remote.Augmenter;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static org.awaitility.Awaitility.await;
import static org.hamcrest.CoreMatchers.equalTo;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BaseTest {

    protected static WebDriver driver;
    protected static String projectUrl = "https://fieldlog.mp.mrsdev.ru";
    //protected static String projectUrl = "https://staging.mp.mrsdev.ru";
    //protected static String projectUrl = "https://mrs-fieldlog-app.s3.plotpad.com";
    //protected static String projectUrl = "https://sinara.mp.mrsdev.ru";

    protected static int driverWait = 8;

    public UUID idProject;
    public String nameProject = generateString(4) + "UITest";
    public String nameNewProject;
    HashMap<UUID, String> newProject = new HashMap<>();
    protected static String pathProjects = "/api/projects/projects/";
    //protected static String pathProjects = "/api/projects/";
    protected static int successCode = 200;
    protected static int deleteCode = 204;

    @BeforeAll
    public void beforeTest() {
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("useAutomationExtension", false);
        options.addArguments("--window-size=1920,1080");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(driverWait, TimeUnit.SECONDS);

        driver.get(projectUrl);
        PageLogin user = new PageLogin();
        user.loginAsAdmin();

        PageObject pageObject = new PageObject();
        ProjectInDto dto = new ProjectInDto(null, null, nameProject, null ,"SeleniumTest");
        idProject = pageObject.createProjectApi(dto);
        waiter(idProject, projectUrl + pathProjects, successCode);

        pageObject.openPageProject(idProject, nameProject);
    }

    public void openPageProjectINDto(UUID id) {
        PageObject pageObject = new PageObject();
        pageObject.openPageProject(id, newProject.get(id));
    }

    public UUID createProjectInDto() {
        if (newProject.size()==0) {
            newProject.put(idProject, nameProject);
            return idProject;
        }
        else {
            nameNewProject = generateString(4) + "UITest";
            UUID idNewProject;
            PageObject pageObject = new PageObject();
            ProjectInDto dto = new ProjectInDto(null, null, nameNewProject, null, "SeleniumTest");
            idNewProject = pageObject.createProjectApi(dto);
            waiter(idNewProject, projectUrl + pathProjects, successCode);
            pageObject.openPageProject(idNewProject, nameNewProject);
            newProject.put(idNewProject, nameNewProject);
            return idNewProject;
        }
    }

    public void deleteProjectInDto(UUID id) {
        if (id != idProject) {
            PageObject pageObject = new PageObject();
            pageObject.deleteThroughApi(projectUrl + pathProjects + id);
        }
    }

    @DisplayName("Перезагрузка и возврат к главной странице")
    public void startTest () throws InterruptedException {
        //driver.navigate().refresh();
        driver.get(projectUrl);
        //Thread.sleep(2000);
    }

    public String generateString(int maxlength) {
        return RandomStringUtils.randomAlphabetic(maxlength);
    }

    protected String generateStringInt(int maxlength) {
        return RandomStringUtils.randomNumeric(maxlength);
    }

    public String AuthToken () {
        WebStorage webStorage = (WebStorage) new Augmenter().augment(driver);
        String authToken = webStorage.getLocalStorage().getItem("AUTH_TOKEN");
        return authToken;
    }

    public void waiter(UUID id, String path, int statusCode) {
        await()
                .atMost(Duration.TEN_SECONDS)
                .with()
                .pollInterval(Duration.FIVE_HUNDRED_MILLISECONDS)
                .until(() -> given()
                                .auth().oauth2(AuthToken())
                                .contentType(ContentType.JSON)
                                .accept(ContentType.JSON)
                                .get(path + id.toString()).getStatusCode(),
                        equalTo(statusCode));
    }

    @AfterAll
    public void tearDownWebDriver() {
        PageObject pageObject = new PageObject();
        pageObject.deleteThroughApi(projectUrl + pathProjects + idProject);
        //if (idSecondProject != null) pageObject.deleteThroughApi(baseUrlProjectApi + pathProjects + idSecondProject);
        /*if (driver != null) {
            driver.quit();
        }*/
    }

}

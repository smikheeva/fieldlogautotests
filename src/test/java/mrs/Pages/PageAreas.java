package mrs.Pages;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.UUID;

public class PageAreas extends PageObject {
  public PageAreas() { super(); }

  Actions builder = new Actions(driver);

  @FindBy(xpath = "//*[@data-ui-test='addNewObject']")
  public WebElement addObjectBtn;

  @FindBy(xpath = "//*[@data-ui-test='objectName']")
  public WebElement objectNameField;

  @FindBy(xpath = "//*[@data-ui-test='oblectAddress']")
  public WebElement objectAddressField;

  @FindBy(xpath = "//button[contains(.,'Создать объект')]")
  public WebElement addThisObject;

  @FindBy(xpath = "//button[contains(.,'Отмена')]")
  public WebElement cancelCreateObject;

  @FindBy(xpath = "//*[@data-ui-test='createArea']")
  public WebElement createAreaBtn;

  @FindBy(xpath = "//*[@data-ui-test='updateArea']")
  public WebElement updateAreaBtn;

  @FindBy(xpath = "//*[@data-ui-test='deleteArea']")
  public WebElement deleteAreaBtn;

  @FindBy(xpath = "//*[@data-ui-test='areaName']")
  public WebElement areaNameField;

  public String areaActionSelection = "../../*[@data-ui-test='areaActionSelection']";
  public String areasTitle = "//*[@data-ui-test='areaTitle']";
  public String objectName;
  public String objectAddress;
  public String areaName;
  public String editObjectName;
  public String editObjectAddress;
  public String editAreaName;


  public void openAreasSidebar(UUID idProject) {
    driver.get(projectUrl + "/#/project/" + idProject + "/subproject-list");
    WebDriverWait wait = new WebDriverWait(driver, driverWait);
    wait.until(ExpectedConditions.urlToBe(projectUrl + "/#/project/" + idProject + "/subproject-list"));
  }

  public void createObjectDisable() {
    Assertions.assertEquals(false, addObjectBtn.isEnabled());
  }

  public void createObject() {
    waitForElementNotPresented(loader);
    addObjectBtn.isDisplayed();
    addObjectBtn.click();
    objectName = "UITestArea" + generateString(6);
    objectNameField.sendKeys(objectName);
    objectAddress = generateString(5);
    objectAddressField.sendKeys(objectAddress);
    driver.findElement(By.xpath("//p[text()='Создание объекта']")).click();
    addThisObject.click();
    getWhenVisible(By.xpath(areasTitle + "[text() = '" + objectName + "']"));
  }

  public void cancelCreateObject() {
    waitForElementNotPresented(loader);
    addObjectBtn.isDisplayed();
    addObjectBtn.click();
    objectName = generateString(6);
    objectNameField.sendKeys(objectName);
    cancelCreateObject.click();
    getWhenNotVisible(By.xpath(areasTitle + "[text()='" + objectName + "']"));
  }

  public void createAreasInObject() {
    int i;
    for (i = 0; i < 5; i++) {
      driver.findElement(By.xpath("//p[text()='" + objectName + "']/"+areaActionSelection)).click();
      createAreaBtn.click();
      areaName = "Участок " + (i + 1);
      areaNameField.sendKeys(areaName);
      areaNameField.sendKeys(Keys.ENTER);
      waitForElementNotPresented(loader);
      getWhenVisible(By.xpath("//p[text()='Участок " + (i + 1) + "']"));
    }
  }

  public void createAreaInObject() {
    waitForElementPresence(driver.findElement(By.xpath(areasTitle + "[text() = '"+ objectName+ "']/"+areaActionSelection+"")));
    driver.findElement(By.xpath(areasTitle + "[text()='"+ objectName+"']/"+areaActionSelection+"")).click();
    createAreaBtn.click();
    areaName = generateString(7);
    areaNameField.sendKeys(areaName);
    areaNameField.sendKeys(Keys.ENTER);
    getWhenVisible(By.xpath("//p[text()='"+areaName+"']"));
  }

  public void createAreaIntoArea(int i) {
    driver.findElement(By.xpath(areasTitle + "[text()='"+ objectName+ "']/"+areaActionSelection+"")).click();
    createAreaBtn.click();
    areaNameField.sendKeys("1 участок");
    areaNameField.sendKeys(Keys.ENTER);
    for (int j = 1; j < i; j++) {
      driver.findElement(By.xpath("//p[text()='" + j + " участок']/../..//button")).click();
      createAreaBtn.click();
      int a = j + 1;
      areaNameField.sendKeys("" + a + " участок");
      areaNameField.sendKeys(Keys.ENTER);
      getWhenVisible(By.xpath("//p[text()='" + j + " участок']"));
    }
  }

  public void checkAreasLevels(Integer areaNumber) {
    getWhenVisible(By.xpath("//p[text()='" + areaNumber + " участок']"));
  }

  public void checkObjectInfo() {
    getWhenVisible(By.xpath(areasTitle + "[text()='" + objectName + "']"));
    getWhenVisible(By.xpath("//p[text()='" + objectAddress + "']"));
  }

  public void updateObject() {
    getWhenVisible(By.xpath(areasTitle + "[text()='" + objectName + "']/"+areaActionSelection+"")).click();
    updateAreaBtn.click();
    objectNameField.clear();
    editObjectName = "UITestUpdateArea" + generateString(8);
    objectNameField.sendKeys(editObjectName);
    objectAddressField.clear();
    editObjectAddress = generateString(6);
    objectAddressField.sendKeys(editObjectAddress);
    driver.findElement(By.xpath("//button[contains(.,'Сохранить изменения')]")).click();
    objectName = editObjectName;
    objectAddress = editObjectAddress;
  }

  public void updateArea() {
    getWhenVisible(By.xpath("//p[text()='" + areaName + "']/"+areaActionSelection+"")).click();
    updateAreaBtn.click();
    editAreaName = generateString(6);
    areaNameField.sendKeys(deleteString + editAreaName);
    areaNameField.sendKeys(Keys.ENTER);
    getWhenVisible(By.xpath("//*[text()='" + editAreaName + "']"));
    areaName = editAreaName;
  }

  public void deleteArea() {
    driver.findElement(By.xpath("//p[text()='" + areaName + "']/"+areaActionSelection+"")).click();
    deleteAreaBtn.click();
    waitForElementNotPresented(By.xpath("//p[text()='" + areaName + "']"));
  }

  public void deleteObject() {
    driver.findElement(By.xpath(areasTitle + "[text()='" + objectName + "']/"+areaActionSelection+"")).click();
    deleteAreaBtn.click();
    waitForElementNotPresented(By.xpath(areasTitle + "[text()='" + objectName + "']"));
  }
}

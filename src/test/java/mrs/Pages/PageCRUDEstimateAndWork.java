package mrs.Pages;

import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.By.xpath;

public class PageCRUDEstimateAndWork extends PageObject {
  public PageCRUDEstimateAndWork() { super();}

  Actions builder = new Actions(driver);

  //@FindBy(xpath = "//button//span[text()='Создать ведомость']")
  @FindBy(xpath = "//*[@data-ui-test='createEstimate']")
  public WebElement createEstimateBtn;

  //@FindBy(xpath = "//input[@name = 'title']")
  @FindBy(xpath = "//*[@data-ui-test='estimateName']")
  public WebElement estimateNameField;

  //@FindBy(xpath = "//input[@name = 'code']")
  @FindBy(xpath = "//*[@data-ui-test='estimateNumber']")
  public WebElement estimateNumberField;

  //@FindBy(xpath = "//div[contains(@class, 'spacing')]//button//span[text()='Создать ведомость']")
  @FindBy(xpath = "//*[@data-ui-test='createThisEstimate']")
  public WebElement createThisEstimateBtn;

  //@FindBy(xpath = "//button//span[text()='Сохранить изменения']")
  @FindBy(xpath = "//*[@data-ui-test='updateThisInfo']")
  public WebElement updateEstimateInfoBtn;

  @FindBy(xpath = "//button//span[text()='Сохранить изменения']")
  public WebElement updateWorkInfoBtn;

  //@FindBy(xpath = "//button//span[text()='Отмена']")
  @FindBy(xpath = "//*[@data-ui-test='cancelUpdateThisInfo']")
  public WebElement cancelUpdateEstimateBtn;

  //@FindBy(xpath = "//div//span[text()='Отмена']")
  @FindBy(xpath = "//*[@data-ui-test='cancelCreateThisEstimate']")
  public WebElement cancelCreateEstimateBtn;

  @FindBy(xpath = "//*[@data-ui-test='mainWorksTab']")
  public WebElement mainWorksTab;

  @FindBy(xpath = "//*[@data-ui-test='additionalWorksTab']")
  public WebElement additionalWorksTab;

  //@FindBy(xpath = "//button[contains(.,'Создать работу')]")
  @FindBy(xpath = "//*[@data-ui-test='createOneMoreAddWork']")
  public WebElement createWorkBtn;

  @FindBy(xpath = "//*[@data-ui-test='applyAddWork']")
  public WebElement applyAddNewWork;

  @FindBy(xpath = "//div//span[text()='Отмена']")
  //@FindBy(xpath = "//*[@data-ui-test='cancelCreateWork']")
  public WebElement cancelCreateWork;

  //@FindBy(xpath = "//input[@name='title']")
  @FindBy(xpath = "//*[@data-ui-test='additionalWorkName']")
  public WebElement workNameField;

  //@FindBy(xpath = "//input[@name='valueTotal']")
  @FindBy(xpath = "//*[@data-ui-test='additionalWorkValue']")
  public WebElement workValueField;

  @FindBy(xpath = "//div[@role='button']")
  public WebElement workMeasureField;

  //@FindBy(xpath = "//div[contains (@class, 'titleContainer')]/following-sibling::div//button")
  @FindBy(xpath = "//*[@data-ui-test='actionsWithEstimate']")
  public WebElement actionsWithEstimate;

  //@FindBy(xpath = "//div[contains (@class, 'rowEstimateDetailedContainer')]//button")
  @FindBy(xpath = "//*[@data-ui-test='actionsWithWork']")
  public WebElement actionsWithWork;

  //@FindBy(xpath = "//li//p[text()='Редактировать']")
  @FindBy(xpath = "//*[@data-ui-test='editEstimate']")
  public WebElement editEstimateBtn;

  //@FindBy(xpath = "//li//p[text()='Редактировать']")
  @FindBy(xpath = "//*[@data-ui-test='editWork']")
  public WebElement editWorkBtn;

  //@FindBy(xpath = "//li//p[text()='Удалить']")
  @FindBy(xpath = "//*[@data-ui-test='deleteEstimate']")
  public WebElement deleteEstimateBtn;

  //@FindBy(xpath = "//li//p[text()='Удалить']")
  @FindBy(xpath = "//*[@data-ui-test='deleteWork']")
  public WebElement deleteWorkBtn;

  @FindBy(xpath = "//*[@data-ui-test='applyConfirmWork']")
  public WebElement applyDeleteBtn;

  @FindBy(xpath = "//*[@data-ui-test='confirmWork']")
  public WebElement confirmWork;

  @FindBy(xpath = "//*[@data-ui-test='applyConfirmWork']")
  public WebElement applyConfirmWorksButton;

  @FindBy(xpath = "//ul")
  public WebElement sidebar;

  @FindBy(xpath = "//li/p[text()='ПСД']")
  public WebElement sidebarPSD;

  @FindBy(xpath = "//li/p[text()='Прогресс работ']")
  public WebElement sidebarPW;

  public String createdEstimateName;
  public String createdEstimateNumber;
  public String editedEstimateName;
  public String createdWorkName;
  public String createdWorkValue;
  public String editedWorkName;

  public String estimateTitleList        = "//*[@data-ui-test='estimateTitle']//div[@class='firstRow--Hj82p1GU']//p[1]";

  @DisplayName("Создание ведомости")
  public void createEstimate() {
    waitForElementNotPresented(loader);
    createEstimateBtn.click();
    createdEstimateName = "TUI" + generateString(5);
    estimateNameField.sendKeys(createdEstimateName);
    createdEstimateNumber = generateStringInt(6);
    estimateNumberField.sendKeys(createdEstimateNumber);
    estimateNameField.click();//временное решение. Кнопка становится активной при смене фокуса
    createThisEstimateBtn.click();
    waitForElementNotPresented(loader);
  }

  @DisplayName("Создание ведомости")
  public void createEstimateForRoles(String estimateName) {
    waitForElementNotPresented(loader);
    createEstimateBtn.click();
    estimateNameField.sendKeys(estimateName);
    createdEstimateNumber = generateStringInt(6);
    estimateNumberField.sendKeys(createdEstimateNumber);
    estimateNameField.click();//временное решение. Кнопка становится активной при смене фокуса
    createThisEstimateBtn.click();
    getWhenVisible(By.xpath("//p[text()='"+estimateName+"']"));
  }

  @DisplayName("Отмена создания ведомости")
  public void cancelCreateEstimate () {
    waitForElementNotPresented(loader);
    createEstimateBtn.click();
    createdEstimateName = generateString(8);
    estimateNameField.sendKeys(createdEstimateName);
    createdEstimateNumber = generateStringInt(6);
    estimateNumberField.sendKeys(createdEstimateNumber);
    cancelCreateEstimateBtn.click();
    waitForElementPresence(createEstimateBtn);
    Assertions
        .assertEquals(0, driver.findElements(By.xpath("//p[text()='" + createdEstimateName + "']")).size());
  }

  @DisplayName("Выбор сметы в списке по имени")
  public void selectEstimateByName(String estimateName) {
    builder.moveToElement(sidebar).click(sidebarPSD).build().perform(); // потом удалить
    waitForElementNotPresented(loader);
    builder.moveToElement(sidebar).click(sidebarPW).build().perform();
    builder.moveToElement(sidebar, 500, 0).click().build().perform();
    driver.findElement(By.xpath("//p[text()='"+estimateName+"']")).click();
  }

  @DisplayName("Выбор работы в списке по имени")
  public void selectWorkByName(String workName) {
    waitForElementNotPresented(loader);
    driver.findElement(By.xpath("//p[text()='"+workName+"']")).click();
  }

  @DisplayName("Заполнение полей при создании работы")
  public void fillingWorkFields () throws InterruptedException {
    createdWorkName = "WORKtest" + generateString(6);
    createdWorkValue = "1" + generateStringInt(2);
    workNameField.sendKeys(createdWorkName);
    workValueField.sendKeys(createdWorkValue);
    workMeasureField.click();
    int randomUnit = (int) (Math.random() * 8) + 1;
    Thread.sleep(1000);
    driver.findElement(By.xpath("//ul[@role='listbox']/li[" + randomUnit + "]")).click();
  }

  @DisplayName("Отменить создание основной работы")
  public void cancelCreateMainWork () throws Exception {
    mainWorksTab.click();
    createWorkBtn.click();
    fillingWorkFields();
    waitForElementPresence(applyAddNewWork);
    Thread.sleep(2000);
    cancelCreateWork.click();
    Assertions.assertEquals(0,driver.findElements(xpath("//div/p[text()='"+ createdWorkName +"']")).size());
  }

  @DisplayName("Создание основной работы")
  public void createMainWork () throws Exception {
    mainWorksTab.click();
    createWorkBtn.click();
    fillingWorkFields();
    waitForElementPresence(applyAddNewWork);
    Thread.sleep(2000);
    applyAddNewWork.click();
    getWhenVisible(By.xpath("//div/p[text()='"+ createdWorkName +"']"));
  }

  @DisplayName("Создание дополнительной работы")
  public void createAddWork () throws Exception {
    var work = new PageWorkProgress();
    additionalWorksTab.click();
    createWorkBtn.click();
    fillingWorkFields();
    waitForElementPresence(applyAddNewWork);
    Thread.sleep(2000);
    applyAddNewWork.click();
    getWhenVisible(By.xpath("//div/p[text()='"+ createdWorkName +"']"));
    driver.findElement(xpath("//*[@data-ui-test='workTitle']//p[text()='" + createdWorkName + "']")) .click();
    waitForElementNotPresented(loader);
    //work.viewSystemMessageAboutNewWork();
  }

  @DisplayName("Создание работы с определенным именем")
  public void createWork (String workName) throws Exception {
    createWorkBtn.click();
    createdWorkValue = "1" + generateStringInt(2);
    workNameField.sendKeys(workName);
    workValueField.sendKeys(createdWorkValue);
    workMeasureField.click();
    int randomUnit = (int) (Math.random() * 8) + 1;
    driver.findElement(By.xpath("//ul[@role='listbox']/li[" + randomUnit + "]")).click();
    waitForElementPresence(applyAddNewWork);
    Thread.sleep(2000);
    applyAddNewWork.click();
    getWhenVisible(By.xpath("//div/p[text()='"+ workName +"']"));
  }

  @DisplayName("Переход к списку основных работ")
  public void openMainWorkList () {
    mainWorksTab.click();
  }

  @DisplayName("Переход к списку дополнительных работ")
  public void openAdditionalWorkList () {
    additionalWorksTab.click();
  }

  @DisplayName("Редактировать ведомости")
  public void updateEstimate () {
    actionsWithEstimate.click();
    editEstimateBtn.click();
    waitForElementPresence(estimateNameField);
    estimateNameField.clear();
    editedEstimateName = "Edit" + generateString(6);
    estimateNameField.sendKeys(editedEstimateName);
    createdEstimateNumber = generateStringInt(4);
    estimateNumberField.clear();
    estimateNumberField.sendKeys(createdEstimateNumber);
    updateEstimateInfoBtn.click();
    getWhenVisible(By.xpath("//p[text()='"+editedEstimateName+"']"));
    getWhenVisible(By.xpath("//p[text()='"+createdEstimateNumber+"']"));
  }

  @DisplayName("Отмена редактирования ведомости")
  public void cancelUpdateEstimate () {
    actionsWithEstimate.click();
    editEstimateBtn.click();
    waitForElementPresence(estimateNameField);
    estimateNameField.clear();
    editedEstimateName = generateString(9);
    estimateNameField.sendKeys(editedEstimateName);
    cancelUpdateEstimateBtn.click();
    waitForElementPresence(createEstimateBtn);
    Assertions
        .assertEquals(driver.findElements(By.xpath("//p[text()='" + editedEstimateName + "']")).size(), 0);
  }

  @DisplayName("Редактировать основную работу с редактированием ед. измерения")
  public void updateMainWorkWithMeasure () throws InterruptedException {
    waitForElementNotPresented(loader);
    actionsWithWork.click();
    editWorkBtn.click();
    waitForElementPresence(workNameField);
    editedWorkName = "WORKtest" + generateString(6);
    createdWorkValue = "1"+ generateStringInt(2);
    workNameField.clear();
    workNameField.sendKeys(editedWorkName);
    workValueField.clear();
    workValueField.sendKeys(createdWorkValue);
    workMeasureField.click();
    Thread.sleep(2000);
    driver.findElement(By.xpath("//li/p[text()='кг']")).click();
    Thread.sleep(2000);
    updateWorkInfoBtn.click();
    getWhenVisible(By.xpath("//div//p[text()='"+editedWorkName+"']"));
    getWhenVisible(By.xpath("//p[text()='Всего по смете']/../p[text()='"+createdWorkValue+"']"));
    getWhenVisible(By.xpath("//p[text()='Всего по смете']/../p[text()='кг']"));
  }

  @DisplayName("Редактировать основную работу без редактированиия ед. измерения")
  public void updateWorkWithoutMeasure() throws InterruptedException {
    String oldWorkMeasure = driver.findElement(By.xpath("//p[text()='Всего по смете']/../p[2]")).getText().split("\\ ", 2)[1];
    System.out.println("Единица измерения: " + oldWorkMeasure);
    waitForElementNotPresented(loader);
    actionsWithWork.click();
    editWorkBtn.click();
    waitForElementPresence(workNameField);
    editedWorkName = "WORKtest" + generateString(6);
    createdWorkValue = "1"+ generateStringInt(2);
    workNameField.clear();
    workNameField.sendKeys(editedWorkName);
    workValueField.clear();
    workValueField.sendKeys(createdWorkValue);
    Thread.sleep(2000);
    updateWorkInfoBtn.click();
    getWhenVisible(By.xpath("//p[text()='"+editedWorkName+"']"));
    getWhenVisible(By.xpath("//p[text()='Всего по смете']/../p[text()='"+createdWorkValue+"']"));
    getWhenVisible(By.xpath("//p[text()='Всего по смете']/../p[text()='"+oldWorkMeasure+"']"));
  }

  @DisplayName("Удаление ведомости работ")
  public void deleteEstimate (String estimateName) {
    actionsWithEstimate.click();
    deleteEstimateBtn.click();
    applyDeleteBtn.click();
    waitForElementNotPresented(By.xpath("//p[text()='" + estimateName + "']"));
  }

  @DisplayName("Удаление работы")
  public void deleteWork (String workName) {
    waitForElementNotPresented(loader);
    actionsWithWork.click();
    deleteWorkBtn.click();
    waitForElementNotPresented(loader);
    Assertions.assertEquals(0, driver.findElements(By.xpath("//p[text()='"+workName+"']")).size());
  }

  @DisplayName("Удаление дополнительной работы")
  public void deleteAddWork (String workName) {
    waitForElementNotPresented(loader);
    actionsWithWork.click();
    deleteWorkBtn.click();
    openAdditionalWorkList();
    waitForElementNotPresented(By.xpath("//div//p[text()='"+workName+"']"));
  }

  @DisplayName("Проверка названия работы в блокноте")
  public void checkWorkNameInNotebook(String workName) {
    getWhenVisible(By.xpath("//div[contains(@class, 'notepadListViewContent')]//p[text()='"+workName+"']"));
  }

  @DisplayName("Проверка названия работы в журнале")
  public void checkWorkNameInJournal(String workName) {
    getWhenVisible(By.xpath("//div[contains(@class, 'journalListView')]//p[text()='"+workName+"']"));
  }

  @DisplayName("Подтверждение работы в блокноте")
  public void confirmWork (String workName) {
    confirmWork.click();
    waitForElementPresence(applyConfirmWorksButton);
    applyConfirmWorksButton.click();
    waitForElementNotPresented(By.xpath("//div[contains(@class, 'notepadListViewContent')]//p[text()='"+workName+"']"));
    getWhenVisible(By.xpath("//div[contains(@class, 'journalListView')]//p[text()='"+workName+"']"));
  }

  @DisplayName("Проверка права/возможости просмотра ведомости")
  public void checkRightViewingEstimates (String estimateName) {
    waitForElementNotPresented(loader);
    getWhenVisible(By.xpath("//p[text()='"+estimateName+"']"));
  }

  @DisplayName("Проверка права/возможости просмотра основной работы")
  public void checkRightViewingMainWork (String mainWorkName) {
    openMainWorkList();
    getWhenVisible(By.xpath("//p[text()='"+mainWorkName+"']"));
  }

  @DisplayName("Проверка права/возможости просмотра дополнительной работы")
  public void checkRightViewingAddWork (String addWorkName) {
    openAdditionalWorkList();
    getWhenVisible(By.xpath("//p[text()='"+addWorkName+"']"));
  }

  @DisplayName("Проверка отсутсвия прав создания/редактирования/удаления ведомости")
  public void checkNoRightForEstimate (String estimateName) {
    Assertions.assertEquals(false, createEstimateBtn.isEnabled());
    selectEstimateByName(estimateName);
    actionsWithEstimate.click();
    Assertions.assertEquals(false, editEstimateBtn.isEnabled());
    Assertions.assertEquals(false, deleteEstimateBtn.isEnabled());
  }

  @DisplayName("Проверка отсутсвия прав создания/редактирования/удаления основной работы")
  public void checkNoRightForMainWork (String mainWorkName) {
    Assertions.assertEquals(false, createWorkBtn.isEnabled());
    selectWorkByName(mainWorkName);
    waitForElementPresence(actionsWithWork);
    actionsWithWork.click();
    Assertions.assertEquals(false, editWorkBtn.isEnabled());
    Assertions.assertEquals(false, deleteWorkBtn.isEnabled());
  }

  @DisplayName("Проверка отсутсвия прав создания/редактирования/удаления дополнительной работы")
  public void checkNoRightForAdditionalWork(String addWorkName) {
    Assertions.assertEquals(false, createWorkBtn.isEnabled());
    selectWorkByName(addWorkName);
    waitForElementPresence(actionsWithWork);
    actionsWithWork.click();
    Assertions.assertEquals(false, editWorkBtn.isEnabled());
    Assertions.assertEquals(false, deleteWorkBtn.isEnabled());
  }

  @DisplayName("Возврат к списку смет")
  public void backToListOfEstimates(String estimateName) {
    driver.findElement(By.xpath("//p[text()='" + estimateName + "']/../../button")).click();
    waitForElementNotPresented(loader);
  }

}

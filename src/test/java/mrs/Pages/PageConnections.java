package mrs.Pages;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;

import java.util.ArrayList;

public class PageConnections extends PageObject{

    public PageConnections () { super (); }

    String area;
    String folder;
    String estimate;

    ArrayList<String> workInNotebookBeforeConfirm    = new ArrayList<>();
    ArrayList<String> volumeInNotebookBeforeConfirm  = new ArrayList<>();
    ArrayList<String> workInJournalBeforeConfirm     = new ArrayList<>();
    ArrayList<String> volumeInJournalBeforeConfirm   = new ArrayList<>();

    public String firstRow = "//div[contains(@class, 'firstRow--')]";
    public String labelContainer = "//div[contains(@class, 'activityItem')]/div[contains(@class, 'labelContainer')]";
    public String infoblock = "//div[contains(@class, 'infoBlock')]";
    public String folderRow = "//span[contains(@class, 'label--')]/p";
    public String areaTitle = "//p[@data-ui-test='areaTitle']";

    public void getFolderInPSD() {
        folder = driver.findElement(By.xpath(folderRow)).getText();
    }

    public void checkFolder() {
        Assertions.assertNotEquals(folder, driver.findElement(By.xpath(folderRow)).getText());
    }

    public void getEstimate() {
        estimate = driver.findElement(By.xpath(firstRow+"/p[1]")).getText();
    }

    public void checkEstimate() {
        Assertions.assertNotEquals(estimate, driver.findElement(By.xpath(firstRow+"/p[1]")).getText());
    }

    public void getWorkInNotebook() {
        for (int i=0;i<driver.findElements(By.xpath(infoblock+"/p")).size();i++){
            workInNotebookBeforeConfirm.add(driver.findElements(By.xpath(infoblock+"/p")).get(i).getText());
            volumeInNotebookBeforeConfirm.add(driver.findElements(By.xpath(infoblock+"/div/p[2]")).get(i).getText());
        }
        for (int i=0;i<driver.findElements(By.xpath(labelContainer+"/p")).size();i++){
            workInJournalBeforeConfirm.add(driver.findElements(By.xpath(labelContainer+"/p")).get(i).getText());
            volumeInJournalBeforeConfirm.add(driver.findElements(By.xpath(labelContainer+"/div/p[2]")).get(i).getText());
        }
    }

    public void checkWorkInNotebook() {
        Assertions.assertEquals(0, driver.findElements(By.xpath(infoblock+"/p")).size());
    }

    public void checkWorkInJournal() {
        Assertions.assertEquals(0, driver.findElements(By.xpath(labelContainer+"/p")).size());
    }

    public void checkArea() {
        Assertions.assertNotEquals(area, driver.findElement(By.xpath(areaTitle)).getText());
    }
}

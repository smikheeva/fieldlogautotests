package mrs.Pages;

import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.UUID;

public class PageDocumentation extends PageObject {
    public PageDocumentation() {
        super();
    }

    Actions builder = new Actions(driver);

    @FindBy(xpath = "//*[@id='startDate']")
    public WebElement startDocumentationPeriod;

    @FindBy(xpath = "//*[@id='endDate']")
    public WebElement endDocumentationPeriod;

    @FindBy(xpath = "//*[text()='Скачать CSV']")
    public WebElement downloadCSV;

    @DisplayName("Перехожу к исполнительной документации")
    public void openDocumentation(UUID idProject) {
        driver.get(projectUrl + "/#/project/" + idProject + "/documentation/");
        WebDriverWait wait = new WebDriverWait(driver, driverWait);
        wait.until(ExpectedConditions.urlToBe(projectUrl + "/#/project/" + idProject + "/documentation/"));
        driver.navigate().refresh();
    }

    @DisplayName("Перехожу к исполнительной документации")
    public void setDate() {
        var work = new PageWorkProgress();
        work.getDateOfPeriod();
        builder.click(startDocumentationPeriod)
                .sendKeys(Keys.LEFT)
                .sendKeys(Keys.LEFT)
                .sendKeys(work.startPeriod)
                .build()
                .perform();
        builder.click(endDocumentationPeriod)
                .sendKeys(Keys.LEFT)
                .sendKeys(Keys.LEFT)
                .sendKeys(work.endPeriod)
                .click()
                .build()
                .perform();
    }

    @DisplayName("Перехожу к исполнительной документации")
    public void downloadDocumentation() {
        //Assertions.assertEquals(false,downloadCSV.isEnabled());
        setDate();
        downloadCSV.click();
        checkDownloadCSV();
    }

    @DisplayName("Проверить наличие файла CSV")
    public void checkDownloadCSV() {
        waitForElementNotPresented(loader);
        File csv = new File("/home/user/Загрузки/report.csv");
        Assertions.assertEquals(true, csv.exists());
    }
}

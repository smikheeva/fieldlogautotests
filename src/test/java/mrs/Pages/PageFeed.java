package mrs.Pages;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.Date;

public class PageFeed extends PageObject{
    public PageFeed () { super (); }
    public int waitTime = 1000;

    private String comment       = generateString(4) + "UITestSendComment";
    private String commentEnter  = generateString(4) + "UITestEnterComment";
    private String editComment   = generateString(4) + "UITestEditComment";
    private String timeComment;

    Actions builder = new Actions(driver);

    /**PageFeed */
    @FindBy(xpath = "//*[@data-ui-test='workCommentField']")
    public WebElement workCommentField;

    @FindBy(xpath = "//*[@data-ui-test='workAddFileToComment']")
    public WebElement workAddFileToComment;

    @FindBy(xpath = "//*[@data-ui-test='workSendComment']")
    public WebElement sendCommentButton;

    //@FindBy(xpath = "//*[@data-ui-test='editComment']")
    @FindBy(xpath = "//div[3]/li")
    public WebElement editCommentButton;

    @FindBy(xpath = "//div[3]/button")
    public WebElement cancelEditCommentButton;

    //@FindBy(xpath = "//*[@data-ui-test='selectSomeComments']")
    @FindBy(xpath = "//div[3]/li[2]")
    public WebElement selectSomeCommentsButton;

    //@FindBy(xpath = "//*[@data-ui-test='deleteComment']")
    @FindBy(xpath = "//div[3]/li[3]")
    public WebElement deleteCommentButton;

    public String commentBlock = "//div[contains(@class, 'commentBlock--')]/";

    @DisplayName("Написать и отправить текстовый комментарий")
    public void sendTextComment (){
        builder.click(workCommentField).sendKeys(comment).click(sendCommentButton).build().perform();
        timeComment = String.format("%tR" ,new Date());
        waitForElementNotPresented(loader);
        checkPresenceComment(comment);
    }

    @DisplayName("Написать и отправить текстовый комментарий с прикрепленным файлом")
    public void sendTextCommentWithFile (String MyComment, String FileName) {
        Actions builder = new Actions(driver);
        builder.click(workCommentField).sendKeys(MyComment).build().perform();
        builder.sendKeys(workAddFileToComment, FileName).build().perform();
        sendCommentButton.click();
    }

    @DisplayName("Редактировать текстовый комментарий")
    public void editComment () {
        driver.findElement(By.xpath(commentBlock + "p[text()='"+comment+"']")).click();
        editCommentButton.click();
        builder.click(workCommentField).sendKeys(workCommentField, deleteString).sendKeys(workCommentField, editComment).build().perform();
        sendCommentButton.click();
        waitForElementNotPresented(loader);
        checkAbsenceComment(comment);
        checkPresenceComment(editComment);
        comment = editComment;
    }

    @DisplayName("Отменить редактирование текстового комментария")
    public void cancelEditComment () {
        driver.findElement(By.xpath(commentBlock + "p[text()='" + comment + "']")).click();
        editCommentButton.click();
        builder.click(workCommentField).sendKeys(workCommentField, deleteString).sendKeys(workCommentField, editComment).build().perform();
        cancelEditCommentButton.click();
        checkAbsenceComment(editComment);
        checkPresenceComment(comment);
    }

    @DisplayName("Удалить текстовый комментарий")
    public void deleteComment () {
        driver.findElement(By.xpath(commentBlock + "p[text()='" + comment + "']")).click();
        waitForElementNotPresented(loader);
        deleteCommentButton.click();
        waitForElementNotPresented(loader);
        checkAbsenceComment(comment);
    }

    @DisplayName("Проверить наличие комментария")
    public void checkPresenceComment(String textComment){
        Assertions.assertEquals(1, driver.findElements(By.xpath(commentBlock + "p[text()='" + textComment + "']")).size());
        Assertions.assertEquals(timeComment, driver.findElement(By.xpath(commentBlock + "p[text()='" + textComment + "']/../../../p")).getText());
    }

    @DisplayName("Проверить отсутствие комментария")
    public void checkAbsenceComment(String textComment){
        Assertions.assertEquals(0, driver.findElements(By.xpath(commentBlock + "p[text()='" + textComment + "']")).size());
    }
}


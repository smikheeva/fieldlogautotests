package mrs.Pages;

import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PageLogin extends PageObject{
    public PageLogin() {super();}

    //public String adminEmail = "smikheeva@plotpad.ru";
    //public String adminPassword = "123qweASD";

    public String adminEmail = "sivanov@plotpad.ru";
    public String adminPassword = "123";

    //public String adminEmail = "test@ya.ru";
    //public String adminPassword = "Qwerty123";

    //public String adminEmail = "Apozdeev@mrspro.ru";
    //public String adminPassword = "qwerty123";

    public String headCQCTEmail = "hcqc@ya.ru";
    public String headCQCTPassword = "Qwerty123";
    public String engineerPTDEmail = "eptd@ya.ru";
    public String engineerPTDPassword = "Qwerty123";
    public String engineerCQCEmail = "ecqc@ya.ru";
    public String engineerCQCPassword = "Qwerty123";
    public String foremanEmail = "foreman@ya.ru";
    public String foremanPassword = "Qwerty123";
    public String IAEmail = "foreman@ya.ru";
    public String IAPassword = "Qwerty123";

    //smikheeva@plotpad.ru/123qweASD
    //tgaranin@plotpad.ru/Qwerty123
    //Apozdeev@mrspro.ru/qwerty123

    @FindBy(xpath = "//div[3]/button")
    public WebElement loginBtn;

    @FindBy(xpath = "//*[@data-ui-test='createProjectButton']")
    public WebElement createNewProjectBtn;

    @FindBy(xpath = "//button//span[text()='Выйти']")
    public WebElement logoutBtn;

    @DisplayName("Зайти как Администратор")
    public void loginAsAdmin() {
        driver.findElement(By.xpath("//input[@id='Email']")).sendKeys(adminEmail);
        driver.findElement(By.xpath("//input[@id='Password']")).sendKeys(adminPassword);
        loginBtn.click();
        waitForElementPresence(createNewProjectBtn);
    }

    @DisplayName("Зайти как Руководитель СК")
    public void loginAsHeadCQCT () {
        driver.findElement(By.xpath("//input[@id='Email']")).sendKeys(headCQCTEmail);
        driver.findElement(By.xpath("//input[@id='Password']")).sendKeys(headCQCTPassword);
        loginBtn.click();
        getWhenVisible(By.xpath("//div[contains(@class, 'projectCardList')]//p[text()='"+nameProject+"']")).click();
    }

    @DisplayName("Зайти как Инженер ПТО")
    public void loginAsEngineerPTD () {
        driver.get(projectUrl);
        driver.findElement(By.xpath("//input[@id='Email']")).sendKeys(engineerPTDEmail);
        driver.findElement(By.xpath("//input[@id='Password']")).sendKeys(engineerPTDPassword);
        loginBtn.click();
        getWhenVisible(By.xpath("//div[contains(@class, 'projectCardList')]//p[text()='"+nameProject+"']")).click();
    }

    @DisplayName("Зайти как Инженер СК")
    public void loginAsEngineerCQC () {
        driver.get(projectUrl);
        driver.findElement(By.xpath("//input[@id='Email']")).sendKeys(engineerCQCEmail);
        driver.findElement(By.xpath("//input[@id='Password']")).sendKeys(engineerCQCPassword);
        loginBtn.click();
        getWhenVisible(By.xpath("//div[contains(@class, 'projectCardList')]//p[text()='"+nameProject+"']")).click();
    }

    @DisplayName("Зайти как Прораб")
    public void loginAsForeman () {
        driver.findElement(By.xpath("//input[@id='Email']")).sendKeys(foremanEmail);
        driver.findElement(By.xpath("//input[@id='Password']")).sendKeys(foremanPassword);
        loginBtn.click();
        getWhenVisible(By.xpath("//div[contains(@class, 'projectCardList')]//p[text()='"+nameProject+"']")).click();
    }

    @DisplayName("Зайти как Исполнитель работ")
    public void loginAsIA () {
        driver.findElement(By.xpath("//input[@id='Email']")).sendKeys(IAEmail);
        driver.findElement(By.xpath("//input[@id='Password']")).sendKeys(IAPassword);
        loginBtn.click();
        getWhenVisible(By.xpath("//div[contains(@class, 'projectCardList')]//p[text()='"+nameProject+"']")).click();
    }

    @DisplayName("Зайти как Руководитель СК")
    public void loginIvanov () {
        System.out.println(nameProject);
        driver.findElement(By.xpath("//input[@id='Email']")).sendKeys("sivanov@plotpad.ru");
        driver.findElement(By.xpath("//input[@id='Password']")).sendKeys("123");
        loginBtn.click();
        waitForElementPresence(createNewProjectBtn);
    }

    @DisplayName("Выйти")
    public void logout () {
        driver.get(projectUrl + "/#/profile");
        logoutBtn.click();
        driver.get(projectUrl);
    }

}

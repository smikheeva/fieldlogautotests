package mrs.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class PageNavigation extends PageObject {

    public PageNavigation() {
        super();
    }

    @FindBy(xpath = "//ul")
    public WebElement sidebar;

    @FindBy(xpath = "//div[contains(@class, 'projectCardBlock--32SZbcz8')]")
    public WebElement listOfProjectsSidebar;

    @FindBy(xpath = "//*[@data-id='myPage']")
    public WebElement myPageSidebar;

    @FindBy(xpath = "//*[@data-id='psd']")
    public WebElement PSDSidebar;

    @FindBy(xpath = "//*[@data-id='estimates']")
    public WebElement estimatesSidebar;

    @FindBy(xpath = "//*[@data-id='journal']")
    public WebElement journalSidebar;

    @FindBy(xpath = "//*[@data-id='documentation']")
    public WebElement IDSidebar;

    @FindBy(xpath = "//*[@data-id='subprojects']")
    public WebElement subprojectsSidebar;

    @FindBy(xpath = "//*[@data-id='profile']")
    public WebElement profileSidebar;

    Actions builder = new Actions(driver);

    public void OpenListOfProjectsSidebar () {
        builder.moveToElement(sidebar).moveToElement(listOfProjectsSidebar).click(listOfProjectsSidebar).build().perform();
        waitForElementPresence(driver.findElement(By.xpath("//button//span[text()='Добавить проект']")));
    }

    public void OpenPSDSidebar () {
        builder.moveToElement(sidebar).moveToElement(PSDSidebar).click(PSDSidebar).build().perform();
        waitForElementPresence(driver.findElement(By.xpath("//button//span[text()='Загрузить файл']")));
    }

    public void OpenEstimatesSidebar() {
        builder.moveToElement(sidebar).moveToElement(estimatesSidebar).click(estimatesSidebar).build().perform();
        waitForElementPresence(driver.findElement(By.xpath("//p[text()='У вас нет смет']")));
    }

    public void OpenJournalSidebar () {
        builder.moveToElement(sidebar).moveToElement(journalSidebar).click(journalSidebar).build().perform();
        waitForElementPresence(driver.findElement(By.xpath("//*[@data-ui-test='confirmTodayWorks']")));
    }

    public void OpenDocumentationSidebar () {
        builder.moveToElement(sidebar).moveToElement(IDSidebar).click(IDSidebar).build().perform();
        waitForElementPresence(driver.findElement(By.xpath("//div[contains(@class, 'uploadDataCardContent--')]")));
    }

    public void OpenSubprojectsSidebar () {
        builder.moveToElement(sidebar).moveToElement(subprojectsSidebar).click(subprojectsSidebar).build().perform();
        waitForElementPresence(driver.findElement(By.xpath("//button//span[text()='Добавить объект']")));
    }

    public void OpenMyPageSidebar () {
        builder.moveToElement(sidebar).moveToElement(myPageSidebar).click(myPageSidebar).build().perform();
        waitForElementPresence(driver.findElement(By.xpath("//div/p[text()='Моя страница']")));
    }

    public void OpenProfileSidebar () {
        builder.moveToElement(sidebar).moveToElement(profileSidebar).click(profileSidebar).build().perform();
        waitForElementPresence(driver.findElement(By.xpath("//button//span[text()='Выйти']")));
    }

}

package mrs.Pages;

import lombok.var;
import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class PageNotebook extends PageObject {
    public PageNotebook () { super (); }

    ArrayList<String> workInNotebookBeforeConfirm    = new ArrayList<>();
    ArrayList<String> volumeInNotebookBeforeConfirm  = new ArrayList<>();
    ArrayList<String> workInJournalBeforeConfirm     = new ArrayList<>();
    ArrayList<String> volumeInJournalBeforeConfirm   = new ArrayList<>();
    ArrayList<String> workInJournalAfterConfirm      = new ArrayList<>();
    ArrayList<String> volumeInJournalAfterConfirm    = new ArrayList<>();

    public String infoBlock                          = "//div[contains(@class, 'infoBlock')]";
    public String lastConfirmedDataValueContainer    = "//div[contains(@class, 'lastConfirmedDataValueContainer--')]";
    public String labelContainer                     = "//div[contains(@class, 'activityItem')]/div[contains(@class, 'labelContainer')]";
    public String counterInNotebook                  = "//div[contains(@class, 'message--1yeXxk_9')]/p";
    public String countWorks                         = "//div[contains(@class, 'countWorks')]/p";
    public String emptyJournal                       = "//div[contains(@class, 'emptyJournalEstimate--16IgxDM7')]/p";
    public String emptyNotebook                      = "//div[contains(@class, 'emptyContainer--t7HcHDqt')]/p";

    @FindBy(xpath = "//*[@data-ui-test='confirmWork']")
    public WebElement confirmWork;

    @FindBy(xpath = "//*[@data-ui-test='applyConfirmWork']")
    public WebElement applyConfirmWorksButton;

    @FindBy(xpath = "//*[contains(text(),'Нет')]")
    public WebElement cancelButton;

    @DisplayName("Проверка записей в журнале на начало смены")
    public void startWorkDay() {
        Assertions.assertEquals("На сегодня нет подтвержденных работ", driver.findElement(By.xpath(emptyJournal)).getText());
        Assertions.assertEquals("Вы подтвердили выполнение 0 работ " + getToday(), driver.findElement(By.xpath(emptyNotebook)).getText());
    }

    @DisplayName("Перехожу к блокноту/журналу")
    public void openNotebook (UUID idProject) {
        driver.get(projectUrl + "/#/project/"+ idProject + "/journal/");
        WebDriverWait wait = new WebDriverWait(driver, driverWait);
        wait.until(ExpectedConditions.urlToBe(projectUrl + "/#/project/"+ idProject + "/journal/"));
        //driver.navigate().refresh();
    }

    @DisplayName("Получаю работы в блокноте до подтверждения")
    public void getWorkInNotebookBeforeConfirm () {
        workInNotebookBeforeConfirm.clear();
        volumeInNotebookBeforeConfirm.clear();
        for (int i=0;i<driver.findElements(By.xpath(infoBlock+"/p")).size();i++){
            workInNotebookBeforeConfirm.add(driver.findElements(By.xpath(infoBlock+"/p")).get(i).getText());
            volumeInNotebookBeforeConfirm.add(driver.findElements(By.xpath(infoBlock+"/div/p[2]")).get(i).getText());
        }
    }

    @DisplayName("Получаю работы в журнале до подтверждения")
    public void getWorkInJournalBeforeConfirm () {
        workInJournalBeforeConfirm.clear();
        volumeInJournalBeforeConfirm.clear();
        if (driver.findElements(By.xpath(labelContainer +"/p")).size() == 0) {
            for (int i=0;i<workInNotebookBeforeConfirm.size();i++){
                workInJournalBeforeConfirm.add("0.0");
                volumeInJournalBeforeConfirm.add("0.0");
            }
        }
        else for (int i=0;i<driver.findElements(By.xpath(labelContainer +"/p")).size();i++){
            workInJournalBeforeConfirm.add(driver.findElements(By.xpath(labelContainer +"/p")).get(i).getText());
            volumeInJournalBeforeConfirm.add(driver.findElements(By.xpath(labelContainer + "/div/p[2]")).get(i).getText());
        }
    }

    @DisplayName("Получаю работы в журнале после подтверждения")
    public void getWorkAfterConfirm () {
        workInJournalAfterConfirm.clear();
        volumeInJournalAfterConfirm.clear();
        for (int i=0;i<driver.findElements(By.xpath(labelContainer +"/p")).size();i++){
            workInJournalAfterConfirm.add(driver.findElements(By.xpath(labelContainer +"/p")).get(i).getText());
            volumeInJournalAfterConfirm.add(driver.findElements(By.xpath(labelContainer + "/div/p[2]")).get(i).getText());
        }
    }

    @DisplayName("Отменяю подтверждение работ и проверяю соответствие значений")
    public void cancelConfirmWork ()  {
        getWorkInNotebookBeforeConfirm();
        confirmWork.click();
        waitForElementPresence(cancelButton);
        cancelButton.click();
        for (int i=0;i<workInNotebookBeforeConfirm.size();i++){
            Assertions.assertEquals(workInNotebookBeforeConfirm.get(i), driver.findElements(By.xpath(infoBlock+"/p")).get(i).getText());
        }
        Assertions.assertEquals(0, driver.findElements(By.xpath(labelContainer +"/p")).size());
    }

    @DisplayName("Подтвердить выполнение отмеченных работ")
    public void confirmWorkDisable () {
        Assertions.assertEquals(false, confirmWork.isEnabled());
    }

    @DisplayName("Подтвердить выполнение отмеченных работ")
    public void confirmWork () {
        getWorkInNotebookBeforeConfirm();
        getWorkInJournalBeforeConfirm();
        confirmWork.click();
        waitForElementPresence(applyConfirmWorksButton);
        applyConfirmWorksButton.click();
        waitForElementNotPresented(loader);
        getWorkAfterConfirm();
        int count=0;
        for (int i=0;i<workInNotebookBeforeConfirm.size();i++){
            for (int j=0;j<volumeInJournalAfterConfirm.size();j++){
                System.out.println("Сравниваем работы: " + workInNotebookBeforeConfirm.get(i) + " и " + workInJournalAfterConfirm.get(j));
                System.out.println("Сравниваем объёмы: " + (Double.parseDouble(volumeInJournalBeforeConfirm.get(i)) + Double.parseDouble(volumeInNotebookBeforeConfirm.get(i))) + " и " + volumeInJournalAfterConfirm.get(j));
                System.out.println();
                if (workInNotebookBeforeConfirm.get(i).equals(workInJournalAfterConfirm.get(j)) &&
                        Precision.round(Double.parseDouble(volumeInNotebookBeforeConfirm.get(i)) +
                                Double.parseDouble(volumeInJournalBeforeConfirm.get(j)), 3) ==
                                Precision.round(Double.parseDouble(volumeInJournalAfterConfirm.get(j)), 3))
                    count++;
            }
        }
        Assertions.assertEquals(0, driver.findElements(By.xpath(infoBlock+"/p")).size());
        Assertions.assertEquals(workInNotebookBeforeConfirm.size(), count);
    }

    @DisplayName("Проверяю соответствие записи в журнале и в детализации работы, системное сообщение о выполненном объёме")
    public void checkedSystemMessage () {
        var work = new PageWorkProgress();
        work.selectRandomEstimate();
        for (int i=0;i<workInNotebookBeforeConfirm.size();i++) {
            waitForElementPresence(driver.findElement(By.xpath("//*[contains(text(),'" + workInNotebookBeforeConfirm.get(i) + "')]")));
            driver.findElement(By.xpath("//*[contains(text(),'" + workInNotebookBeforeConfirm.get(i) + "')]")).click();
            Assertions.assertEquals(1, driver.findElements(By.xpath("//*[contains(text(),'выполнил " + (Double.parseDouble(volumeInNotebookBeforeConfirm.get(i))) + "')]")).size());
            Assertions.assertEquals(volumeInNotebookBeforeConfirm.get(i), driver.findElement(By.xpath(lastConfirmedDataValueContainer+"/p[1]")).getText());
            work.returnToListOfEstimates();
        }
    }

    @DisplayName("Проверяю соответствие записи в журнале и в детализации работы, системное сообщение о выполненном объёме")
    public void checkCountWorks () {
        //waitForElementNotPresented(By.xpath("//p[text()='За сегодня вы отметили прогресс по следующим работам']"));
        System.out.println(driver.findElement(By.xpath(countWorks)).getText());
        int count  = Integer.parseInt(driver.findElement(By.xpath(countWorks)).getText().split(" ", 2)[0]);
        Assertions.assertEquals(count, driver.findElements(By.xpath(infoBlock)).size());
    }

    @DisplayName("Клик по работе в блокноте")
    public void clickOnWorkInNotebook () {
        var work = new PageWorkProgress();
        workInNotebookBeforeConfirm.clear();
        for (int i=0;i<driver.findElements(By.xpath(infoBlock+"/p")).size();i++){
            workInNotebookBeforeConfirm.add(driver.findElements(By.xpath(infoBlock+"/p")).get(i).getText());
        }
        work.workTitle = workInNotebookBeforeConfirm.get((int) Math.random() * workInNotebookBeforeConfirm.size());
        driver.findElement(By.xpath("//p[text()='"+work.workTitle+"']")).click();
        Assertions.assertEquals(work.workTitle, driver.findElement(By.xpath(work.workTitleInDetailed)).getText());
    }

    @DisplayName("Очистка блокнота")
    public void clearNotebook() {
        confirmWork.click();
        applyConfirmWorksButton.click();
        waitForElementNotPresented(loader);
    }

    @DisplayName("получаю сегодняшнюю дату")
    public String getToday() {
        String todayMonth = "";
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        String todayDay = Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
        switch (month) {
            case 0:
                todayMonth = "января";
                break;
            case 1:
                todayMonth = "февраля";
                break;
            case 2:
                todayMonth = "марта";
                break;
            case 3:
                todayMonth = "апреля";
                break;
            case 4:
                todayMonth = "мая";
                break;
            case 5:
                todayMonth = "июня";
                break;
            case 6:
                todayMonth = "июля";
                break;
            case 7:
                todayMonth = "августа";
                break;
            case 8:
                todayMonth = "сентября";
                break;
            case 9:
                todayMonth = "октября";
                break;
            case 10:
                todayMonth = "ноября";
                break;
            case 11:
                todayMonth = "декабря";
                break;
        }
        String today = todayDay.concat(" ").concat(todayMonth).concat(", ");
        return today;
    }
}

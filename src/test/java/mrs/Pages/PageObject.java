package mrs.Pages;

import com.plotpad.service.projects.api.dto.ProjectInDto;
import io.restassured.http.ContentType;
import mrs.BaseTest;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.UUID;

import static io.restassured.RestAssured.given;

public class PageObject extends BaseTest {
    public By loader = By.className("//div[contains(@class, 'MuiCircularProgress')]");

   public PageObject() {
       PageFactory.initElements(driver, this);}

   public UUID createProjectApi(ProjectInDto dto) {
       return given()
             .auth().oauth2(AuthToken())
             .contentType(ContentType.JSON)
             .body(dto)
             .when()
             .post(projectUrl + pathProjects)
             .then()
             .statusCode(200)
             .extract()
             .as(UUID.class);
   }

   public void openPageProject(UUID id, String nameProject) {
       driver.get(projectUrl + "/#/project-list");
       WebDriverWait wait = new WebDriverWait(driver, driverWait);
       wait.until(ExpectedConditions.urlToBe(projectUrl + "/#/project-list"));
       driver.navigate().refresh();
       waitForElementNotPresented(loader);
       getWhenVisible(By.xpath("//div[contains(@class, 'projectCardList')]//p[text()='"+nameProject+"']")).click();
       wait.until(ExpectedConditions.urlToBe(projectUrl + "/#/project/"+ id + "/project-info"));
   }

   @DisplayName("Ждет, когда элемент появится на странице")
   public void waitForElementPresence(WebElement element) {
       new WebDriverWait(driver, driverWait)
               .until(ExpectedConditions.visibilityOf(element));
   }

   @DisplayName("Ждет, что элемента на странице не будет")
   public void waitForElementNotPresented(By locator) {
       new WebDriverWait(driver, driverWait)
               .until(ExpectedConditions.numberOfElementsToBe(locator, 0));
   }

   public static String deleteString = Keys.chord(Keys.CONTROL, "a") + Keys.DELETE;

   @DisplayName("Wait for element is presented in a page. Option 2")
   public WebElement getWhenVisible(By locator) {
       WebElement element = null;
       WebDriverWait wait = new WebDriverWait(driver, driverWait);
       element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
       return element;
   }

   public Boolean getWhenNotVisible(By locator) {
       WebDriverWait wait = new WebDriverWait(driver, driverWait);
       Boolean element = wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
       return element;
   }

   public void deleteThroughApi(String url) {
       given()
            .auth().oauth2(AuthToken())
            .contentType(ContentType.JSON)
            .accept(ContentType.JSON).when()
            .when()
            .delete(url)
            .then()
            .assertThat()
            .statusCode(deleteCode);
   }
}

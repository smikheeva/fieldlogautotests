package mrs.Pages;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PageProject extends PageObject {
    public PageProject () { super (); }

    private ArrayList<String> nameEmployee= new ArrayList<>();
    String projectName;
    private  String nameOrganization;
    private  String typeOrganization;
    int numberOrganization;

    @FindBy(xpath = "//*[@data-ui-test='createProjectButton']")
    public WebElement createNewProjectBtn;

    @FindBy(xpath = "//*[contains(text(),'Отмена')]")
    public WebElement cancelButton;

    @FindBy(xpath = "//*[@data-ui-test='confirmCreateProject']")
    public WebElement confirmCreateProjectButton;

    @FindBy(xpath = "//*[@data-ui-test='projectName']")
    public WebElement nameProject;

    @FindBy(xpath = "//*[@data-ui-test='buildingAddress']")
    public WebElement addressProject;

    //@FindBy(xpath = "//*[@data-ui-test='updateProject']")
    @FindBy(xpath = "//*[@data-ui-test='updateArea']")
    public WebElement editProjectButton;

    @FindBy(xpath = "//*[@data-ui-test='addParticipantsToProject']")
    public WebElement addParticipantsToProjectButton;

    //@FindBy(xpath = "//*[@data-ui-test='deleteProject']")
    @FindBy(xpath = "//*[@data-ui-test='deleteArea']")
    public WebElement deleteProjectButton;

    @FindBy(xpath = "//*[@data-ui-test='saveChangesProject']")
    public WebElement saveEditProjectButton;

    @FindBy(xpath = "//*[@id='app']/div/main/div/div/div[1]/div/button")
    public WebElement addParticipantToProjectButtonInParticipants;

    @FindBy(xpath = "//div[2]/div[3]/div/div[2]/div/div[1]/div/div/div")
    public WebElement chooseOrganization;

    @FindBy(xpath = "//div[2]/div[3]/div/div[2]/div/div[2]/div/div/div/div[1]")
    public WebElement chooseTypeOrganization;

    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[1]")
    public WebElement cancelAddOrganization;

    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[2]")
    public WebElement confirmAddOrganization;

    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[1]")
    public WebElement cancelAddEmployeeButton;

    @FindBy(xpath = "//div[2]/div[3]/div/div[3]/div/button[2]")
    public WebElement confirmAddEmployeeButton;

    @FindBy(xpath = "//li")
    public WebElement sidebar;

    public String activeProjectTitle = "//div[contains(@class, 'titleBlock--3ni01Bbk')]/p[1]";
    public String projectTitle = "//div[contains(@class, 'title--3q_t4nwe')]/p";

    Actions builder = new Actions(driver);

    @DisplayName("Открыть страницу проектов")
    public void openProjectPage(UUID idProject) {
        driver.get(projectUrl + "/#/project/"+ idProject + "/project-list/");
        WebDriverWait wait = new WebDriverWait(driver, driverWait);
        wait.until(ExpectedConditions.urlToBe(projectUrl + "/#/project-list"));
        waitForElementNotPresented(loader);
    }

    @DisplayName("Кнопка создания проекта недоступна")
    public void createProjectDisable() {
        Assertions.assertEquals(false, createNewProjectBtn.isEnabled());
    }

    @DisplayName("Создать проект")
    public void createProject() {
        projectName ="UITestProject" + generateString(4);
        waitForElementNotPresented(loader);
        createNewProjectBtn.click();
        builder.click(nameProject).sendKeys(projectName).build().perform();
        builder.click(addressProject).sendKeys("Я сам удалю").build().perform();
        confirmCreateProjectButton.click();
        waitForElementNotPresented(loader);
        checkProject(projectName);
    }

    @DisplayName("Отменить создание проекта")
    public void cancelCreateProject() {
        projectName = generateString(4) + "UITestProject";
        waitForElementNotPresented(loader);
        createNewProjectBtn.click();
        builder.click(nameProject).sendKeys(projectName).build().perform();
        builder.click(addressProject).sendKeys("Я сам удалю").build().perform();
        cancelButton.click();
    }

    @DisplayName("Выбрать проект")
    public void chooseProject(String nameProject) {
        driver.findElement(By.xpath("//p[text()='"+nameProject+"']")).click();
        assertEquals(nameProject, driver.findElement(By.xpath("//div[contains(@class, 'title--')]/p")).getText());
    }

    @DisplayName("Вызвать меню действий над проектом")
    public void actionProject() {
        driver.findElement(By.xpath("//p[text()='" + projectName + "']/../../../button[@data-ui-test='projectActionSelection']")).click();
    }

    @DisplayName("Редактировать проект и отменить")
    public void cancelEditProjectAction() {
        String editProjectName = generateString(4) + "UITestProject";
        actionProject();
        editProjectButton.click();
        nameProject.click();
        nameProject.clear();
        nameProject.sendKeys(editProjectName);
        cancelButton.click();
        waitForElementNotPresented(loader);
        checkProject(projectName);
        checkAbsenceProject(editProjectName);
    }

    @DisplayName("Редактировать проект и сохранить изменения")
    public void saveEditProjectAction() {
        String editProjectName = "UIEditTestProject" + generateString(4);
        actionProject();
        editProjectButton.click();
        nameProject.click();
        nameProject.sendKeys(deleteString);
        nameProject.sendKeys(editProjectName);
        saveEditProjectButton.click();
        waitForElementNotPresented(loader);
        checkAbsenceProject(projectName);
        checkProject(editProjectName);
        projectName = editProjectName;
    }

    @DisplayName("Удалить проект")
    public void deleteProject() {
        actionProject();
        deleteProjectButton.click();
        waitForElementNotPresented(loader);
        checkAbsenceProject(projectName);
    }

    @DisplayName("Проверить активный проект")
    public void checkActiveProject(){
        builder.moveToElement(sidebar).perform();
        Assertions.assertEquals
                (driver.findElements(By.xpath(projectTitle)).get(0).getText(),
                        driver.findElement(By.xpath(activeProjectTitle)).getText() );
        builder.moveByOffset(1000, 0).perform();
    }

    @DisplayName("Выбрать проект")
    public void selectProject(String project){
        System.out.println(project);
        driver.findElement(By.xpath(projectTitle+"[text()='"+project+"']")).click();
        waitForElementNotPresented(loader);
        Assertions.assertEquals(project, driver.findElement(By.xpath(projectTitle)).getText());
    }

    @DisplayName("Добавить участников к проекту")
    public void addParticipantsAction() {
        actionProject();
        addParticipantsToProjectButton.click();
    }

    @DisplayName("Заполнить форму добавления участников к проекту")
    public void fillingAddParticipantToProject() {
        waitForElementPresence(addParticipantToProjectButtonInParticipants);
        waitForElementNotPresented(loader);
        addParticipantToProjectButtonInParticipants.click();
        waitForElementPresence(chooseOrganization);
        chooseOrganization.click();
        int numberOfOrganization = (int) (Math.random()*3)+1;
        waitForElementNotPresented(loader);
        driver.findElement(By.xpath("//div[@id='react-select-2-option-"+numberOfOrganization+"']/div/div")).click();
        waitForElementPresence(driver.findElement(By.xpath("//div[@id='react-select-2-option-"+numberOfOrganization+"']/div/div")));
        nameOrganization=driver.findElement(By.xpath("//div[@id='react-select-2-option-"+numberOfOrganization+"']/div/div")).getText();
        chooseTypeOrganization.click();
        int numberOfTypeOrganization = (int) (Math.random()*7)+1;
        waitForElementNotPresented(loader);
        waitForElementPresence(driver.findElement(By.xpath("//div[@id='react-select-3-option-"+numberOfTypeOrganization+"']/div/div/div/p")));
        driver.findElement(By.xpath("//div[@id='react-select-3-option-"+numberOfTypeOrganization+"']/div/div/div/p")).click();
        typeOrganization=driver.findElement(By.xpath("//div[@id='react-select-3-option-"+numberOfTypeOrganization+"']/div/div/div/p")).getText();
    }

    @DisplayName("Подтвердить добавление участников к проекту")
    public void confirmAddParticipantToProject() {
        confirmAddOrganization.click();
        int count = 0;
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        for (int i=0;i<listOrganization.size();i++) {
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[" + (i + 1) + "]/div[1]/div/p[1]")).getText().equals(nameOrganization)
                    && driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[" + (i + 1) + "]/div[1]/div/p[2]")).getText().equals(typeOrganization)) {
                count++;
            }
        }
        if (count>0) System.out.println("Добавлена организация '" + nameOrganization + "' в качестве " + typeOrganization);
        else System.out.println("Организация не добавлена");
    }

    @DisplayName("Отменить добавление участников к проекту")
    public void cancelAddParticipantToProject() {
        cancelAddOrganization.click();
        int count = 0;
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        for (int i=0;i<listOrganization.size();i++) {
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[" + (i + 1) + "]/div[1]/div/p[1]")).getText().equals(nameOrganization)
                    && driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div[" + (i + 1) + "]/div[1]/div/p[2]")).getText().equals(typeOrganization)) {
                count++;
            }
        }
        if (count==0) System.out.println("Отмена добавление участников к проекту прошла успешно");
        else System.out.println("Отмена добавление участников к проекту не удалась");
    }

    @DisplayName("Выбрать некоторых сотрудиков для добавления")
    public void chooseEmployeeForAdding() {
        waitForElementNotPresented(loader);
        int count=0;
        ArrayList<Integer> freeNumber= new ArrayList<>();
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        System.out.println("Всего организаций - " + listOrganization.size());
        numberOrganization = (int) (Math.random() * (listOrganization.size()) + 1);
        System.out.println("numberOrganization - " + numberOrganization);
        driver.findElement(By.xpath("//div/div/div[2]/div["+numberOrganization+"]/div[2]/div[1]/button")).click();
        List<WebElement> listEmployee = driver.findElements(By.xpath("//div[2]/div[3]/div/div[2]/div/div/div[2]/div/span"));
        for (int i=0;i<listEmployee.size();i++) {
            freeNumber.add(i+1);
        }
        int amountEmployee = (int) (Math.random()*(listEmployee.size())+1); //сколько сотрудников добавить
        for (int i=0;i<amountEmployee;i++) {
            int numberEmployee = (int) (Math.random() * freeNumber.size() + 1);
            nameEmployee.add(driver.findElement(By.xpath("//div[2]/div[3]/div/div[2]/div/div/div[2]/div[" + freeNumber.get(numberEmployee-1) + "]/div/div[2]/p[1]")).getText());
            for (int j=0;j<freeNumber.size();j++)
                if (freeNumber.get(j)== numberEmployee-1) freeNumber.remove(i);
            driver.findElement(By.xpath("//div[2]/div[3]/div/div[2]/div/div/div[2]/div[" + numberEmployee + "]/span/span[1]/input")).click();
        }
    }

    @DisplayName("Выбрать всех сотрудиков для добавления")
    public void chooseAllEmployeeForAdding() {
        waitForElementNotPresented(loader);
        int count=0;
        ArrayList<Integer> freeNumber= new ArrayList<>();
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        numberOrganization = (int) (Math.random()*listOrganization.size()+1);
        System.out.println("numberOrganization - " + numberOrganization);
        driver.findElement(By.xpath("//div/div/div[2]/div["+numberOrganization+"]/div[2]/div[1]/button")).click();
        List<WebElement> listEmployee = driver.findElements(By.xpath("//div[2]/div[3]/div/div[2]/div/div/div[2]/div/span"));
        driver.findElement(By.xpath("///div[2]/div[3]/div/div[2]/div/div/div[1]/span/span[1]/input")).click();
        for (int i=0;i<listEmployee.size();i++) {
            int numberEmployee = (int) (Math.random() * freeNumber.size() + 1);
            nameEmployee.add(driver.findElement(By.xpath("//div[2]/div[3]/div/div[2]/div/div/div[2]/div[" + freeNumber.get(numberEmployee-1) + "]/div/div[2]/p[1]")).getText());
        }
    }

    @DisplayName("Отмена добавления сотрудников")
    public void cancelAddEmployee() {
        int count=0;
        cancelAddEmployeeButton.click();
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        for (int i=0;i<listOrganization.size();i++) {
            for (int j=0;j<nameEmployee.size();j++){
                if (nameEmployee.get(i).equals(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+(i+1)+"]/div[3]/div/div/div/div/div[2]/p[1]")).getText())){
                    count++;
                }
            }
        }
        if (count>0) System.out.println("Отмена добавления сотрудников прошла неудачно");
    }

    @DisplayName("Подтвердить добавления сотрудников")
    public void confirmAddEmployee() {
        int count=0;
        confirmAddEmployeeButton.click();
        for (int i=0;i<nameEmployee.size();i++){
            if (nameEmployee.get(i).equals(driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+numberOrganization+"]/div[3]/div/div/div/div["+(i+1)+"]/div[2]/p[1]")).getText())){
                count++;
            }
        }
        if (count==nameEmployee.size()) System.out.println("Добавление сотрудников прошло успешно");
    }

    @DisplayName("Редактировать организацию-участника")
    public void editParticipant() {

    }

    @DisplayName("Отменить удаление организации-участника")
    public void cancelDeleteParticipant() {
        int count = 0;
        String deleteOrganization;
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        numberOrganization = (int) (Math.random() * (listOrganization.size()) + 1);
        deleteOrganization = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+numberOrganization+"]/div[1]/div/p[1]")).getText();
        driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+numberOrganization+"]/div[1]/button")).click();
        driver.findElement(By.xpath("//*[@id='fade-menu']/div[3]/div/div/div[2]")).click();
        driver.findElement(By.xpath("//div[2]/div[3]/div/div[3]/div/button[2]")).click();
        for (int i=0;i<listOrganization.size();i++){
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+(i+1)+"]/div[1]/div/p[1]")).getText().equals(deleteOrganization)){
                count++;
            }
        }
        if (count==1) System.out.println("Отмена удаления '" +deleteOrganization+ "' прошла успешно");
        else System.out.println("Отмена удаления потерпела неудачу");
    }

    @DisplayName("Подтвердить удаление организации-участника")
    public void confirmDeleteParticipant() {
        int count = 0;
        String deleteOrganization;
        List<WebElement> listOrganization = driver.findElements(By.xpath("//*[contains(text(),'Добавить сотрудников')]"));
        numberOrganization = (int) (Math.random() * (listOrganization.size()) + 1);
        deleteOrganization = driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+numberOrganization+"]/div[1]/div/p[1]")).getText();
        driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+numberOrganization+"]/div[1]/button")).click();
        driver.findElement(By.xpath("//*[@id='fade-menu']/div[3]/div/div/div[2]")).click();
        driver.findElement(By.xpath("//div[2]/div[3]/div/div[3]/div/button[1]")).click();
        waitForElementNotPresented(loader);
        for (int i=0;i<listOrganization.size();i++){
            if (driver.findElement(By.xpath("//*[@id='app']/div/main/div/div/div[2]/div["+(i+1)+"]/div[1]/div/p[1]")).getText().equals(deleteOrganization)){
                count++;
            }
        }
        System.out.println(count);
        if (count==0) System.out.println("Удаление '" +deleteOrganization+ "' прошло успешно");
        else System.out.println("Удаление потерпело неудачу");
    }

    @DisplayName("Проверить наличие записи проекта")
    public void checkProject (String checkName) {
        System.out.println("Проверяю наличие проекта: " + checkName);
        Assertions.assertEquals(1, driver.findElements(By.xpath(projectTitle+"[text()='" + checkName + "']")).size());
    }

    @DisplayName("Проверить отсутствие записи проекта")
    public void checkAbsenceProject(String checkName) {
        System.out.println("Проверяю отсутствие проекта: " + checkName);
        Assertions.assertEquals(0, driver.findElements(By.xpath("//p[text()='" + checkName + "']")).size());
    }

    public void checkThatTheProjectIsActive (String title) {
        List<WebElement> list = driver.findElements(By.xpath("//div/p[contains(.,'"+title+"')]"));
        assertThat(2, equalTo(list.size()));
    }

}

package mrs.Pages;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PagePsd extends PageObject {

    public PagePsd () { super (); }

    Actions builder = new Actions(driver);

    private String nameFolder;
    private String fileName;

    @FindBy(xpath = "//*[@data-ui-test='createFolderInPSD']")
    public WebElement createFolderInPSD;

    @FindBy(xpath = "//*[@data-ui-test='btn-showMoreGroup']")
    public WebElement actionsFolderInPSD;

    @FindBy(xpath = "//p[contains(.,'Редактировать')]")
    public WebElement editFolderInPSD;

    @FindBy(xpath = "//p[contains(.,'Добавить')]")
    public WebElement addFolderInPSD;

    @FindBy(xpath = "//p[contains(.,'Удалить')]")
    public WebElement deleteFolderInPSD;

    @FindBy(xpath = "//input[@type='file']")
    public WebElement loadFileField;

    @FindBy(xpath = "//*[@data-ui-test='importFilesArps']")
    public WebElement importFilesButton;

    @FindBy(xpath = "//*[@data-ui-test='goToProgressWorkYes']")
    public WebElement goToProgressWorkYes;

    @FindBy(xpath = "//*[@data-ui-test='deleteFiles']")
    public WebElement deleteFilesButton;

    @FindBy(xpath = "//p[text()='Выбрать все']/ancestor::div[contains(@class, 'labelCheckbox')]//input")
    public WebElement checkAllFilesBtn;

    @FindBy(xpath = "//ul")
    public WebElement sidebar;

    @FindBy(xpath = "//li/p[text()='ПСД']")
    public WebElement sidebarPSD;

    @FindBy(xpath = "//li/p[text()='Прогресс работ']")
    public WebElement sidebarPW;

    public String folderRow = "//span[contains(@class, 'label--')]/p";
    public String fileRow = "//div[contains(@class, 'list--')]/div[contains(@class, 'container--')]/p[1]";
    public String estimateTitleList = "//*[@data-ui-test='estimateTitle']//div[@class='firstRow--Hj82p1GU']//p[1]";


    @DisplayName("Открыть страницу проектно-сметной документации")
    public void openPSD(UUID idProject){
        driver.get(projectUrl + "/#/project/"+ idProject + "/psd/");
        WebDriverWait wait = new WebDriverWait(driver, driverWait);
        wait.until(ExpectedConditions.urlToBe(projectUrl + "/#/project/"+ idProject + "/psd/"));
        waitForElementNotPresented(loader);
    }

    @DisplayName("Функции псд не доступны")
    public void psdDisable(){
        Assertions.assertEquals(false,createFolderInPSD.isEnabled());
        Assertions.assertEquals(false,loadFileField.isEnabled());
    }

    @DisplayName("Создать новую папку")
    public void createNewFolder(){
        waitForElementNotPresented(By.className("css937"));
        waitForElementPresence(createFolderInPSD);
        createFolderInPSD.click();
        nameFolder = generateString(4) + "UITestFolder";
        builder.sendKeys(nameFolder).sendKeys(Keys.ENTER).build().perform();
        waitForElementNotPresented(loader);
        assertTrue(getWhenVisible(By.xpath(""+folderRow+"[text()='" + nameFolder + "']")).isDisplayed());
        System.out.println(nameFolder);
    }

    @DisplayName("Выбрать папку и открыть")
    public void selectFolder(){
        driver.findElement(By.xpath(""+folderRow+"[text()='" + nameFolder + "']")).click();
        waitForElementNotPresented(loader);
        getWhenVisible(By.xpath(""+folderRow+"[text()='" + nameFolder + "']"));
    }

    @DisplayName("Редактировать папку")
    public void editFolder(){
        driver.findElement(By.xpath(folderRow+"[text()='" + nameFolder + "']")).click();
        waitForElementNotPresented(loader);
        actionsFolderInPSD.click();
        waitForElementPresence(editFolderInPSD);
        editFolderInPSD.click();
        builder.sendKeys("EditFolder").sendKeys(Keys.ENTER).build().perform();
        nameFolder = nameFolder.concat("EditFolder");
        waitForElementNotPresented(loader);
        Assertions.assertEquals(1,
                driver.findElements(
                        By.xpath(""+folderRow+"[text()='" + nameFolder + "']")).size());
    }

    @DisplayName("Удалить папку")
    public void deleteFolder(){
        driver.findElement(By.xpath(folderRow+"[text()='" + nameFolder + "']")).click();
        waitForElementNotPresented(loader);
        actionsFolderInPSD.click();
        waitForElementPresence(deleteFolderInPSD);
        deleteFolderInPSD.click();
        waitForElementNotPresented(loader);
        Assertions.assertEquals(
                driver.findElements(By.xpath(""+folderRow+"[text()='" + nameFolder + "']")).size(), 0);
    }

    @DisplayName("Добавить папку в папку")
    public void addFolderToFolder(){
        String addFolder = generateString(4) + "AddFolder";
        driver.findElement(By.xpath(folderRow+"[text()='" + nameFolder + "']")).click();
        waitForElementNotPresented(loader);
        actionsFolderInPSD.click();
        waitForElementPresence(addFolderInPSD);
        addFolderInPSD.click();
        driver.findElement(By.xpath(folderRow+"[text()='" + nameFolder + "']")).click(); //потом убрать эту строку
        builder.sendKeys(addFolder).sendKeys(Keys.ENTER).build().perform();
        waitForElementNotPresented(loader);
        Assertions.assertEquals(
                1, driver.findElements(By.xpath(""+folderRow+"[text()='" + addFolder + "']")).size());
    }

    @DisplayName("Загрузить файл в папку")
    public void loadFile(File estimateName)  {
        loadFileField.sendKeys(estimateName.getAbsolutePath());
        fileName = estimateName.getName();
        waitForElementNotPresented(loader);
        System.out.println("Загружаю " + fileName);
        driver.navigate().refresh();
        waitForElementNotPresented(loader);
        driver.findElement(By.xpath(folderRow+"[text()='" + nameFolder + "']")).click();
        waitForElementNotPresented(loader);
        Assertions.assertEquals
                (1, driver.findElements(By.xpath(fileRow+"[text()='" + fileName + "']")).size());
    }

    @DisplayName("Загрузить несколько файлов в папку")
    public void loadSomeEstimates(File estimateLink1, File estimateLink2, File estimateLink3){
        loadFileField.sendKeys(estimateLink1.getAbsolutePath());
        waitForElementNotPresented(loader);
        driver.navigate().refresh();
        waitForElementNotPresented(loader);
        driver.findElement(By.xpath(folderRow+"[text()='" + nameFolder + "']")).click();
        waitForElementNotPresented(loader);
        loadFileField.sendKeys(estimateLink2.getAbsolutePath());
        waitForElementNotPresented(loader);
        driver.navigate().refresh();
        waitForElementNotPresented(loader);
        driver.findElement(By.xpath(folderRow+"[text()='" + nameFolder + "']")).click();
        waitForElementNotPresented(loader);
        loadFileField.sendKeys(estimateLink3.getAbsolutePath());
        waitForElementNotPresented(loader);
        driver.navigate().refresh();
        waitForElementNotPresented(loader);
        driver.findElement(By.xpath(folderRow+"[text()='" + nameFolder + "']")).click();
        waitForElementNotPresented(loader);
        System.out.println("Загружаю " + estimateLink1.getName());
        System.out.println("Загружаю " + estimateLink2.getName());
        System.out.println("Загружаю " + estimateLink3.getName());
        Assertions.assertEquals
                (1, driver.findElements(By.xpath(fileRow+"[text()='" + estimateLink1.getName() + "']")).size());
        Assertions.assertEquals
                (1, driver.findElements(By.xpath(fileRow+"[text()='" + estimateLink2.getName() + "']")).size());
        Assertions.assertEquals
                (1, driver.findElements(By.xpath(fileRow+"[text()='" + estimateLink3.getName() + "']")).size());
    }

    @DisplayName("Удалить  файл из папки")
    public void deleteFile(){
        driver.findElement(By.xpath("//p[1][text()='" + fileName + "']")).click();
        deleteFilesButton.click();
        driver.navigate().refresh();
        waitForElementNotPresented(loader);
        driver.findElement(By.xpath("//p[text()='" + nameFolder + "']")).click();
        Assertions.assertEquals(0,
                driver.findElements(By.xpath(""+fileRow+"[text()='" + fileName + "']")).size());
    }

    @DisplayName("Выбрать и импортировать смету")
    public void importEstimate(){
        driver.findElement(By.xpath("//p[text()='" + fileName + "']")).click();
        waitForElementNotPresented(loader);
        importFilesButton.click();
        goToProgressWorkYes.click();
        waitForElementNotPresented(loader);
        builder.moveToElement(sidebar).click(sidebarPSD).build().perform();
        waitForElementNotPresented(loader);
        builder.moveToElement(sidebar).click(sidebarPW).build().perform();
        waitForElementNotPresented(loader);
        builder.moveToElement(sidebar, 500, 0).click().build().perform();
        String estimate = fileName.replace(".arp", "");
        driver.findElement(By.xpath(estimateTitleList+"[text()='" + estimate + "']")).click();
        System.out.println(estimate);
        waitForElementNotPresented(loader);
        Assertions.assertEquals(2, driver.findElements(By.xpath("//p[text()='" + estimate + "']")).size());
    }

    @DisplayName("Выбрать и импортировать несколько сметы")
    public void importSomeEstimate(File estimateLink1, File estimateLink2, File estimateLink3) {
        driver.findElement(By.xpath("//p[text()='" + estimateLink1.getName() + "']")).click();
        checkAllFilesBtn.click();
        importFilesButton.click();
        waitForElementNotPresented(loader);
        waitForElementPresence(goToProgressWorkYes);
        goToProgressWorkYes.click();
        waitForElementNotPresented(loader);
        builder.moveToElement(sidebar).click(sidebarPSD).build().perform();
        waitForElementNotPresented(loader);
        builder.moveToElement(sidebar).click(sidebarPW).build().perform();
        builder.moveToElement(sidebar, 500, 0).click().build().perform();
        waitForElementNotPresented(loader);
        checkTheEstimate(estimateLink1);
        checkTheEstimate(estimateLink2);
        checkTheEstimate(estimateLink3);
    }

    @DisplayName("Проверить наличие ")
    public void checkTheEstimate(File estimateLink) {
        Assertions.assertEquals(1, driver.findElements(By.xpath(estimateTitleList + "[text()='" + estimateLink.getName().replace(".arp", "") + "']")).size());
    }
}
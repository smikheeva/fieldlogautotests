package mrs.Pages;

import lombok.var;
import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class PageValidation extends PageObject {

    public PageValidation() { super(); }

    Actions builder = new Actions(driver);

    String errorProjectName    = "//p[text()='Введите название проекта (6-200 символов)']";
    String errorProjectAddress = "//p[text()='Введите адрес проекта (2-150 символов)']";
    String errorWorkName       = "//p[text()='Введите название работы (6-300 символов).']";
    String errorEstimateName   = "//p[text()='Введите название ведомости (6-200 символов).']";
    String errorEstimateCode   = "//p[text()='Введите код ведомости (2-50 символов).']";
    String activeColor         = "rgb(67, 167, 226)";

    @FindBy(xpath = "//*[@data-ui-test='estimateTitle']")
    public WebElement estimateTitle;

    @FindBy(xpath = "//*[@data-ui-test='createProjectButton']")
    public WebElement createNewProjectBtn;

    @FindBy(xpath = "//*[contains(text(),'Отмена')]")
    public WebElement cancelButton;

    @FindBy(xpath = "//*[@data-ui-test='confirmCreateProject']")
    public WebElement confirmCreateProjectButton;

    @FindBy(xpath = "//*[@data-ui-test='projectName']")
    public WebElement nameProject;

    @FindBy(xpath = "//*[@data-ui-test='buildingAddress']")
    public WebElement addressProject;

    @FindBy(xpath = "//button//span[text()='Создать ведомость']")
    public WebElement createEstimateBtn;

    @FindBy(xpath = "//input[@name = 'title']")
    public WebElement estimateNameField;

    @FindBy(xpath = "//input[@name = 'code']")
    public WebElement estimateCodeField;

    @FindBy(xpath = "//div[contains(@class, 'spacing')]//button//span[text()='Создать ведомость']/../..")
    public WebElement createThisEstimateBtn;

    @FindBy(xpath = "//button[contains(.,'Создать работу')]")
    public WebElement createWorkBtn;

    @FindBy(xpath = "//*[@data-ui-test='applyAddWork']")
    public WebElement applyAddNewWork;

    @FindBy(xpath = "//input[@name='title']")
    public WebElement workNameField;

    @FindBy(xpath = "//input[@name='valueTotal']")
    public WebElement workValueField;

    @FindBy(xpath = "//*[contains(@class, 'inputSelect')]")
    public WebElement workMeasureField;

    @FindBy(xpath = "//*[@data-ui-test='additionalWorkComment']")
    public WebElement workCommentField;

    public void authorizationValidation() {
    }

    @DisplayName("Проверка наличия элемента по xpath'у на странице")
    public void checkDisplayed(boolean tf, String str) {
        Assertions.assertEquals(tf, driver.findElements(By.xpath(str)).size()==1);
    }

    @DisplayName("Проверка цвета элемента элемента")
    public void checkColorActive(boolean tf, WebElement webElement) {
        var work = new PageWorkProgress();
        Assertions.assertEquals(tf, activeColor.equals(work.getColor(webElement)));
    }

    @DisplayName("Заполнить поле формы создания проекта с указанием поля и длины строки")
    public void fillingFieldProject(WebElement field, int lengthStr) {
        String str = "u" + generateString(lengthStr-1);
        builder.click(field).sendKeys(str).
                moveToElement(field,0,20).click().build().perform();
    }

    @DisplayName("Заполнить поле формы создания ведомости с указанием поля и длины строки")
    public void fillingFieldEstimate(WebElement field, int lengthStr) {
        String str = "u" + generateString(lengthStr-1);
        builder.click(field).sendKeys(str).
                moveToElement(estimateCodeField,0,20).click().build().perform();
    }

    @DisplayName("Заполнить поле формы создания работы с указанием поля и длины строки")
    public void fillingFieldWork(WebElement field, int lengthStr) {
        String str;
        if (field==workValueField) str = Double.toString(Precision.round(Math.random(), lengthStr)).replace(",",".");
        else str = "u" + generateString(lengthStr-1);
        builder.click(field).sendKeys(str).
                click(applyAddNewWork).build().perform();
    }

    public void fillingMeasure(){
        waitForElementNotPresented(loader);
        workMeasureField.click();
        waitForElementNotPresented(loader);
        int randomUnit = (int) (Math.random() * 9) + 1;
        driver.findElement(By.xpath("//ul[@role='listbox']/li[" + randomUnit + "]")).click();
        waitForElementNotPresented(loader);
    }

    @DisplayName("Удалить строку из поля")
    public void deleteStr(WebElement field) {
        builder.doubleClick(field).sendKeys(Keys.DELETE).build().perform();
    }

    @DisplayName("Имя проекта - 0 символов, Адрес проекта - 0 символов")
    public void projectName0Address0() {
        createNewProjectBtn.click();
        confirmCreateProjectButton.click();
        checkDisplayed(true, errorProjectName);
        checkDisplayed(true, errorProjectAddress);
    }

    @DisplayName("Имя проекта - 200 символов, Адрес проекта - 0 символов")
    public void projectName200Address0() {
        fillingFieldProject(nameProject, 201);
        Assertions.assertEquals(200, nameProject.getAttribute("value").length());
        checkDisplayed(false, errorProjectName);
        checkDisplayed(true, errorProjectAddress);
        //Assertions.assertEquals("U", String.valueOf(nameProject.getAttribute("value").charAt(0)));
        builder.doubleClick(nameProject).sendKeys(Keys.DELETE).build().perform();
    }

    @DisplayName("Имя проекта - 0 символов, Адрес проекта - 150 символов")
    public void projectName0Address150() {
        fillingFieldProject(addressProject, 151);
        Assertions.assertEquals(150, addressProject.getAttribute("value").length());
        checkDisplayed(true, errorProjectName);
        checkDisplayed(false, errorProjectAddress);
    }

    @DisplayName("Имя проекта - 10 символов, Адрес проекта - 150 символов")
    public void projectName10Address150() {
        fillingFieldProject(nameProject,10);
        checkDisplayed(false, errorProjectName);
        checkDisplayed(false, errorProjectAddress);
        cancelButton.click();

    }

    @DisplayName("Создать ведомость")
    public void createEstimate() throws Exception {
        createEstimateBtn.click();
        waitForElementNotPresented(loader);
        fillingFieldEstimate(estimateNameField,10);
        fillingFieldEstimate(estimateCodeField,10);
        createThisEstimateBtn.click();
        Thread.sleep(4000);
    }

    @DisplayName("Открыть ведомость")
    public void openEstimate() {
        waitForElementNotPresented(loader);
        estimateTitle.click();
    }

    @DisplayName("Имя ведомости - 0 символов, Адрес проекта - 0 символов")
    public void estimateName0Code0() {
        createEstimateBtn.click();
        waitForElementNotPresented(loader);
        createThisEstimateBtn.click();
        checkDisplayed(true, errorEstimateName);
        checkDisplayed(true, errorEstimateCode);
    }

    @DisplayName("Имя ведомости - 0 символов, Адрес проекта - 1 символ")
    public void estimateName0Code1() {
        fillingFieldEstimate(estimateCodeField, 1);
        createThisEstimateBtn.click();
        checkDisplayed(true, errorEstimateName);
        checkDisplayed(true, errorEstimateCode);
        deleteStr(estimateCodeField);
    }

    @DisplayName("Имя ведомости - 0 символов, Адрес проекта - 2 символа")
    public void estimateName0Code2() {
        fillingFieldEstimate(estimateCodeField, 2);
        checkDisplayed(true, errorEstimateName);
        checkDisplayed(false, errorEstimateCode);
        deleteStr(estimateCodeField);
    }

    @DisplayName("Имя ведомости - 0 символов, Адрес проекта - 50 символов")
    public void estimateName0Code50() {
        fillingFieldEstimate(estimateCodeField, 51);
        Assertions.assertEquals(50, estimateCodeField.getAttribute("value").length());
        checkDisplayed(true, errorEstimateName);
        checkDisplayed(false, errorEstimateCode);
    }

    @DisplayName("Имя ведомости - 5 символов, Адрес проекта - 50 символов")
    public void estimateName5Code50() {
        fillingFieldEstimate(estimateNameField, 5);
        //Assertions.assertEquals("U", String.valueOf(estimateNameField.getAttribute("value").charAt(0)));
        checkDisplayed(true, errorEstimateName);
        checkDisplayed(false, errorEstimateCode);
    }

    @DisplayName("Имя ведомости - 6 символов, Адрес проекта - 50 символов")
    public void estimateName6Code50() {
        fillingFieldEstimate(estimateNameField, 6);
        checkDisplayed(false, errorEstimateName);
        checkDisplayed(false, errorEstimateCode);
        deleteStr(estimateNameField);
    }

    @DisplayName("Имя ведомости - 200 символов, Адрес проекта - 50 символов")
    public void estimateName200Code50() {
        fillingFieldEstimate(estimateNameField,201);
        Assertions.assertEquals(200, estimateNameField.getAttribute("value").length());
        checkDisplayed(false, errorEstimateName);
        checkDisplayed(false, errorEstimateCode);
        cancelButton.click();
    }

    @DisplayName("Проверка кнопки 'Создать работу' при заполнения только поля объёма")
    public void workName301Comment4001() {
        fillingFieldWork(workNameField, 301);
        Assertions.assertEquals(300, workNameField.getAttribute("value").length());
        //Assertions.assertEquals("U", String.valueOf(estimateNameField.getAttribute("value").charAt(0)));
        fillingFieldWork(workCommentField, 4001);
        Assertions.assertEquals(4001, workCommentField.getAttribute("value").length());
        deleteStr(workValueField);
    }

    @DisplayName("Проверка кнопки 'Создать работу' при заполнения только поля объёма")
    public void checkRequiredFieldsNewWorkVolume() {
        createWorkBtn.click();
        fillingFieldWork(workValueField, 3);
        checkDisplayed(true, errorWorkName);
        deleteStr(workValueField);
        cancelButton.click();
    }

    @DisplayName("Проверка кнопки 'Создать работу' при заполнения только полей имени и объёма")
    public void checkRequiredFieldsNewWorkNameAndVolume() {
        createWorkBtn.click();
        fillingFieldWork(workNameField, 10);
        fillingFieldWork(workValueField, 3);
        //checkDisplayed(true, errorWorkName);
        deleteStr(workNameField);
        deleteStr(workValueField);
        cancelButton.click();
    }

    @DisplayName("Проверка кнопки 'Создать работу' при заполнения только полей единиц измерения")
    public void checkRequiredFieldsNewWorkMeasure() {
        createWorkBtn.click();
        fillingMeasure();
        checkDisplayed(true, errorWorkName);
        cancelButton.click();
    }

    @DisplayName("Проверка кнопки 'Создать работу' при заполнения только полей единиц измерения и имени")
    public void checkRequiredFieldsNewWorkMeasureAndName() {
        createWorkBtn.click();
        fillingMeasure();
        fillingFieldWork(workNameField, 10);
        //checkDisplayed(true, errorWorkName);
        deleteStr(workNameField);
        cancelButton.click();
    }

    @DisplayName("Проверка кнопки 'Создать работу' при заполнения только полей единиц измерения и объёма")
    public void checkRequiredFieldsNewWorkMeasureAndVolume() {
        createWorkBtn.click();
        fillingMeasure();
        fillingFieldWork(workValueField, 3);
        checkDisplayed(true, errorWorkName);
        deleteStr(workValueField);
        cancelButton.click();
    }

    @DisplayName("Проверка кнопки 'Создать работу' при заполнения всех полей")
    public void checkRequiredFieldsNewWorkAll() {
        createWorkBtn.click();
        fillingMeasure();
        fillingFieldWork(workNameField, 10);
        fillingFieldWork(workValueField, 3);
        checkDisplayed(false, errorWorkName);
        applyAddNewWork.click();
    }
}

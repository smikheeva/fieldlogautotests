package mrs.Pages;

import lombok.var;
import org.apache.commons.math3.util.Precision;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.openqa.selenium.By.xpath;

public class PageWorkProgress extends PageObject {
    public PageWorkProgress() {
        super();
    }
    Date dateStartPeriod;
    Date dateEndPeriod;
    public String startPeriod;
    public String endPeriod;
    public String employee;
    String lastEnteredVolume;
    private Double fullVolumeInDetailWork;
    private Double doneVolumeInDetailWork;
    private Double leftVolumeInDetailWork;
    private int count;
    int responsible;
    Double volume;
    Double editVolume;
    String estimateTitle;
    String workTitle;
    ArrayList<String> estimatesList  = new ArrayList<>();
    ArrayList<String> worksList  = new ArrayList<>();
    Actions builder = new Actions(driver);

    @FindBy(xpath = "//*[@data-ui-test='additionalWorksTab']")
    public WebElement additionalWorksTab;

    @FindBy(xpath = "//*[@data-ui-test='addingWorkRecord']")
    public WebElement addingWorkRecord;

    @FindBy(xpath = "//*[@data-ui-test='cancelAddingRecord']")
    public WebElement cancelAddingRecord;

    @FindBy(xpath = "//*[@data-ui-test='VolumeWork']")
    public WebElement enterVolumeWork;

    @FindBy(xpath = "//*[@data-ui-test='addingWorkRecordToNote']")
    public WebElement sendToNotebook;

    @FindBy(xpath = "//*[@data-ui-test='confirmWork']")
    public WebElement confirmWork;

    @FindBy(xpath = "//*[@data-ui-test='confirmThisRecord']")
    public WebElement confirmThisRecord;

    @FindBy(xpath = "//p[text()='превышение планового объема']")
    public WebElement warningMoreThenPlaned;

    @FindBy(xpath = "//*[@data-ui-test='startEstimatePeriod']")
    public WebElement startEstimatePeriod;

    @FindBy(xpath = "//*[@data-ui-test='endEstimatePeriod']")
    public WebElement endEstimatePeriod;

    @FindBy(xpath = "//*[@data-ui-test='startDateAddWork']")
    public WebElement startDateAddWork;

    @FindBy(xpath = "//*[@data-ui-test='endDateAddWork']")
    public WebElement endDateAddWork;

    @FindBy(xpath = "//*[@data-ui-test='input-select']")
    public WebElement selectResponsibleToEstimate;

    @FindBy(xpath = "//*[@data-ui-test='returnToEstimatesList']")
    public WebElement returnToEstimatesList;

    //@FindBy(xpath = "//div[contains (@class, 'rowEstimateDetailedContainer')]//button")
    @FindBy(xpath = "//*[@data-ui-test='actionsWithWork']")
    public WebElement actionsWithWork;

    //@FindBy(xpath = "//li//p[text()='Редактировать']")
    @FindBy(xpath = "//*[@data-ui-test='editWork']")
    public WebElement editWorkBtn;


    public String lastVolumeInJournal      = "//div[contains(@class, 'lastConfirmedDataValueContainer--')]/p[1]";
    public String estimateTitleInDetailed  = "//div[contains(@class, 'titleAndOptions')]//p[2]";
    public String workTitleInDetailed      = "//div[contains(@class, 'rowEstimateDetailedContainer')]//p[contains(@class, 'title')]";
    public String estimateTitleList        = "//*[@data-ui-test='estimateTitle']//div[@class='firstRow--Hj82p1GU']//p[1]";
    public String estimateDateOfPeriod     = "//div[@class='secondRow--3kuPgDZ6']/p";
    public String workTitleList            = "//div[contains(@class, 'topBlock--sUKb2NfY')]/p";
    public String journalTitle             = "//div[contains(@class, 'container--v1kwXYka')]/div/p";
    public String commentBlock             = "//div[contains(@class, 'messageContainer')]";
    public String activeEstimateColor      = "rgb(230, 244, 251)";
    public String activeWorkColor          = "rgb(246, 251, 254)";
    public String estimates                = "//div[contains(@class, 'container--z-A_moHl')]/div/div/p";
    public String employeeValue            = "//*[contains(@class, 'employeeValue')]";
    public String employeeCard             = "//*[@data-ui-test='div-employeeCard']";


    @DisplayName("Перейти к прогрессу работ")
    public void openProgressWork(UUID idProject) {
        driver.get(projectUrl + "/#/project/" + idProject + "/estimate/list/rows");
        WebDriverWait wait = new WebDriverWait(driver, driverWait);
        wait.until(ExpectedConditions.urlToBe(projectUrl + "/#/project/" + idProject + "/estimate/list/rows"));
        waitForElementNotPresented(loader);
    }

    @DisplayName("Перейти к моей странице")
    public void openMyPage(UUID idProject) {
        driver.get(projectUrl + "/#/project/" + idProject + "/project-info");
        WebDriverWait wait = new WebDriverWait(driver, driverWait);
        wait.until(ExpectedConditions.urlToBe(projectUrl + "/#/project/" + idProject + "/project-info"));
    }

    @DisplayName("Получить список ведомостей")
    public void getEstimates(){
        waitForElementNotPresented(loader);
        for (int i = 0; i < driver.findElements(xpath(estimateTitleList)).size(); i++) {
            estimatesList.add(driver.findElements(xpath(estimateTitleList)).get(i).getText());
        }
    }

    @DisplayName("Получить список работ")
    public void getWorks(){
        waitForElementNotPresented(loader);
        for (int i = 0; i < driver.findElements(xpath(workTitleList)).size(); i++) {
            worksList.add(driver.findElements(xpath(workTitleList)).get(i).getText());
        }
    }

    @DisplayName("Проверить соответствие ведомостей и работ на моей странице и в прогрессе работ ")
    public void checkEstimatesInMyPage() {
        waitForElementNotPresented(loader);
        for (int i = 0; i < estimatesList.size(); i++) {
            Assertions.assertEquals(estimatesList.get(i), driver.findElements(xpath(estimateTitleList)).get(i).getText());
        }
    }

    @DisplayName("Проверить соответствие ведомостей и работ на моей странице и в прогрессе работ ")
    public void checkWorksInMyPage() {
        waitForElementNotPresented(loader);
        for(int i = 0;i<worksList.size();i++){
            Assertions.assertEquals(worksList.get(i), driver.findElements(xpath(workTitleList)).get(i).getText());
        }
    }

    @DisplayName("Клик по ведомости на моей странице")
    public void clickOnEstimateInMyPage() {
        selectRandomEstimate();
        waitForElementNotPresented(loader);
        Assertions.assertEquals(estimateTitle, driver.findElement(xpath(estimateTitleInDetailed)).getText());
    }

    @DisplayName("Клик по работе на моей странице")
    public void clickOnWorkInMyPage() {
        selectRandomWork();
        waitForElementNotPresented(loader);
        Assertions.assertEquals(workTitle, driver.findElement(xpath(workTitleInDetailed)).getText());
    }

    @DisplayName("Заполнить поле добавления записи")
    public void fillingWorkRecord() {
        waitForElementNotPresented(loader);
        addingWorkRecord.click();
        waitForElementPresence(enterVolumeWork);
        calculateRemainingVolume();
        enterVolumeWork.clear();
        volume = 0.0;
        while (volume < 0.001) {volume = (Precision.round(Math.random() * leftVolumeInDetailWork, 3));}
        enterVolumeWork.sendKeys(Double.toString(volume));
        System.out.println(volume);
    }

    @DisplayName("Ввести новое значнеи в поле добавления записи")
    public void fillingEditRecord () throws InterruptedException {
        addingWorkRecord.click();
        waitForElementPresence(enterVolumeWork);
        calculateRemainingVolume();
        enterVolumeWork.clear();
        editVolume = 0.0;
        while (editVolume < 0.001) {
            editVolume = (Precision.round(Math.random() * leftVolumeInDetailWork, 3));}
        enterVolumeWork.sendKeys(Double.toString(editVolume));
        System.out.println(editVolume);
    }

    @DisplayName("Кнопка 'Добавить запись' о выполнении работы не активна")
    public void addWorkRecordDisable() {
        Assertions.assertEquals(false, addingWorkRecord.isEnabled());
    }

    @DisplayName("Добавить запись о выполнении работы")
    public void addWorkRecord() {
        fillingWorkRecord();
        waitForElementPresence(sendToNotebook);
        sendToNotebook.click();
        waitForElementNotPresented(loader);
        waitForElementPresence(confirmThisRecord);
        lastEnteredVolume =driver.findElement(xpath(""+ lastVolumeInJournal +""))
                .getText()
                .replace(" ", "");
        Assertions.assertEquals(Double.parseDouble(lastEnteredVolume), volume);
    }

    @DisplayName("Нажать кнопку 'Подтвердить' в детализации работ")
    public void confirmThisRecordButtonClick() {
        confirmThisRecord.click();
        waitForElementNotPresented(loader);
        Assertions.assertEquals("Журнал", driver.findElement(xpath(journalTitle)).getText());
    }

    @DisplayName("Редактировать запись о выполнении работы")
    public void updateWorkRecord() throws InterruptedException {
        fillingEditRecord();
        waitForElementPresence(sendToNotebook);
        sendToNotebook.click();
        waitForElementNotPresented(loader);
        waitForElementPresence(confirmThisRecord);
        lastEnteredVolume =driver.findElement(xpath(""+ lastVolumeInJournal +""))
                .getText()
                .replace(" ", "");
        Assertions.assertEquals(lastEnteredVolume, Double.toString(editVolume));
    }

    @DisplayName("Отменить редактирование записи о выполнении работы")
    public void cancelUpdateWorkRecord() throws InterruptedException {
        fillingEditRecord();
        waitForElementPresence(sendToNotebook);
        cancelAddingRecord.click();
        waitForElementNotPresented(loader);
        waitForElementPresence(confirmThisRecord);
        lastEnteredVolume =driver.findElement(xpath(""+ lastVolumeInJournal +"")).getText().replace(" ", "");
        Assertions.assertEquals(lastEnteredVolume, Double.toString(volume));
    }

    @DisplayName("Получить цвет элемента")
    public String getColor(WebElement element) {
        String color = element.getCssValue("background").split( " none", 2)[0];
        return color;
    }

    @DisplayName("Переключение между сметами")
    public void switchingEstimates() {
        driver.findElements(xpath(estimateTitleList)).get(0).click();
        estimateTitle = driver.findElements(xpath(estimateTitleList)).get(0).getText();
        builder.moveToElement(driver.findElement(xpath(estimateTitleInDetailed))).build().perform();
        Assertions.assertEquals(activeEstimateColor, getColor(driver.findElement(xpath(estimateTitleList + "[text()='"+estimateTitle+"']/../../.."))));
        waitForElementNotPresented(loader);
        String work = driver.findElements(xpath(workTitleList)).get(0).getText();
        estimateTitle = driver.findElements(xpath(estimateTitleList)).get(1).getText();
        driver.findElements(xpath(estimateTitleList)).get(1).click();
        waitForElementNotPresented(loader);
        Assertions.assertEquals(false, work.equals(driver.findElements(xpath(workTitleList)).get(0).getText()));
    }

    @DisplayName("Переключение между основными работами")
    public void switchingMainJobs() {
        driver.findElements(xpath(workTitleList)).get(0).click();
        workTitle = driver.findElements(xpath(workTitleList)).get(0).getText();
        waitForElementNotPresented(loader);
        Assertions.assertEquals(activeWorkColor, getColor(driver.findElement(xpath(workTitleList + "[text()='"+workTitle+"']/../../.."))));
        String workTitleInDetailedWork = driver.findElement(xpath(workTitleInDetailed)).getText();
        driver.findElements(xpath(workTitleList)).get(1).click();
        waitForElementNotPresented(loader);
        Assertions.assertEquals(false, workTitleInDetailedWork.equals(driver.findElement(xpath(workTitleInDetailed)).getText()));
        returnToListOfEstimates();
    }

    @DisplayName("Переключение между дополнительными работами")
    public void switchingAddJobs() throws Exception {
        var crude = new PageCRUDEstimateAndWork();
        crude.createAddWork();
        returnToListOfEstimates();
        crude.createAddWork();
        //driver.findElement(xpath("//p[text()='"+crude.createdWorkName+"']")).click();
        driver.findElements(xpath(workTitleList)).get(0).click();
        waitForElementNotPresented(loader);
        String workTitleInDetailedWork = driver.findElement(xpath(workTitleInDetailed)).getText();
        driver.findElements(xpath(workTitleList)).get(1).click();
        waitForElementNotPresented(loader);
        Assertions.assertEquals(false, workTitleInDetailedWork.equals(driver.findElement(xpath(workTitleInDetailed)).getText()));
        returnToListOfEstimates();
    }

    @DisplayName("Переключение между основными и дополнительными работами")
    public void switchingJobsTab() {
        String work = driver.findElements(xpath(workTitleList)).get(0).getText();
        additionalWorksTab.click();
        Assertions.assertEquals(false, work.equals(driver.findElements(xpath(workTitleList)).get(0).getText()));
    }

    @DisplayName("Ввести значение больше планового")
    public void addValueMoreThenPlaned() {
        waitForElementNotPresented(loader);
        addingWorkRecord.click();
        waitForElementPresence(enterVolumeWork);
        enterVolumeWork.clear();
        enterVolumeWork.sendKeys("123456");
        waitForElementPresence(warningMoreThenPlaned);
        Assertions.assertEquals(false, sendToNotebook.isEnabled());
        cancelAddingRecord.click();
    }

    @DisplayName("Отменить введенние записи")
    public void cancelAddingWorkRecord()  {
        fillingWorkRecord();
        waitForElementPresence(cancelAddingRecord);
        cancelAddingRecord.click();
        Assertions.assertEquals(0, count);
    }

    @DisplayName("Проверить наличие работы в блокноте")
    public void checkPresenceWorkInNotebook() {
        count = 0;
        waitForElementPresence(confirmWork);
        if (driver.findElements(xpath("//p[text()='" + workTitle + "']")).size() == 1
                && driver
                .findElements(
                        xpath("//p[text()='" + workTitle + "']/..//p[text()='" + volume + "']"))
                .size()
                == 1) count++;
        Assertions.assertEquals(1, count);
    }

    @DisplayName("Проверить отсутствие работы в блокноте")
    public void checkAbsenceWorkInNotebook() {
        count = 0;
        waitForElementPresence(confirmWork);
        if (driver.findElements(xpath("//p[text()='" + workTitle + "']")).size() == 1
                && driver
                .findElements(
                        xpath("//p[text()='" + workTitle + "']/..//p[text()='" + volume + "']"))
                .size()
                == 1) count++;
        Assertions.assertEquals(0, count);
    }

    @DisplayName("Посчитать оставшийся объем")
    public void calculateRemainingVolume() {
        waitForElementPresence(driver.findElement(xpath("//p[text()='Всего по смете']")));
        fullVolumeInDetailWork =
                Double.parseDouble(
                        (driver.findElement(xpath("//p[text()='Всего по смете']/../p[2]")).getText())
                                .split("\\ ", 2)[0]);
        doneVolumeInDetailWork =
                Double.parseDouble(
                        (driver.findElement(xpath("//p[text()='Выполнено']/../p[2]")).getText())
                                .split("\\ ", 2)[0]);
        leftVolumeInDetailWork = fullVolumeInDetailWork - doneVolumeInDetailWork;
    }

    @DisplayName("Проверка наличия системного сообщения после создания новой дополнительной работы")
    public void viewSystemMessageAboutNewWork() {
        var crud = new PageCRUDEstimateAndWork();
        Assertions.assertEquals(1, driver.findElements(xpath("//p[contains(.,'добавил работу ')]")).size());
        Assertions.assertEquals(1, driver.findElements(xpath(commentBlock + "//p[contains(.,'" + crud.createdWorkName + "')]")).size());
    }

    @DisplayName("Выбрать случайную смету")
    public void selectRandomEstimate() {
        List<WebElement> listEstimates = driver.findElements(xpath("//*[@data-ui-test='estimateTitle']"));
        System.out.println("Всего смет в списке: " + listEstimates.size());
        int randomEstimate = (int) (Math.random() * listEstimates.size());
        listEstimates.get(randomEstimate).click();
        waitForElementNotPresented(loader);
        builder.moveToElement(driver.findElement(xpath(estimateTitleInDetailed))).build().perform();
        estimateTitle = driver.findElement(xpath(estimateTitleInDetailed)).getText();
        System.out.println("Выбранная смета: " + estimateTitle);
        Assertions.assertEquals(activeEstimateColor,getColor(driver.findElement(xpath(estimateTitleList + "[text()='"+estimateTitle+"']/../../.."))));
    }

    @DisplayName("Выбрать случайную работу")
    public void selectRandomWork() {
        List<WebElement> listWorks = driver.findElements(xpath("//*[@data-ui-test='workTitle']"));
        System.out.println("Всего работ в ведомости: " + listWorks.size());
        int randomWork =(int) Math.random() * listWorks.size();
        listWorks.get(randomWork).click();
        waitForElementNotPresented(loader);
        workTitle = driver.findElement(xpath(workTitleInDetailed)).getText();
        System.out.println("Выбранная работа: " + workTitle);
        System.out.println("Цвет активной работы: " + getColor(driver.findElement(xpath(workTitleList + "[text()='"+workTitle+"']/../../.."))));
        Assertions.assertEquals(activeWorkColor, getColor(driver.findElement(xpath(workTitleList + "[text()='"+workTitle+"']/../../.."))));
    }

    @DisplayName("Выбираю рандомную смету и рандомную работу")
    public void selectRandomEstimateAndWork() {
        selectRandomEstimate();
        selectRandomWork();
    }

    @DisplayName("Возврат к списку смет")
    public void returnToListOfEstimates(){
        returnToEstimatesList.click();
        waitForElementNotPresented(loader);
        Assertions.assertEquals("Ведомости работ", driver.findElement(xpath(estimates)).getText());
    }

    @DisplayName("Назначить период выполнения ведомости работ")
    public void addPeriodOfEstimate() {
        getDateOfPeriod();
        builder.click(startEstimatePeriod)
                .sendKeys(Keys.LEFT)
                .sendKeys(Keys.LEFT)
                .sendKeys(startPeriod)
                .build()
                .perform();
        builder.click(endEstimatePeriod)
                .sendKeys(Keys.LEFT)
                .sendKeys(Keys.LEFT)
                .sendKeys(endPeriod)
                .moveToElement(endEstimatePeriod, 100, 100)
                .click()
                .build()
                .perform();
        checkDateOfPeriod();
    }

    @DisplayName("Назначить период выполнения работы")
    public void addPeriodOfWork() {
        actionsWithWork.click();
        editWorkBtn.click();
        getDateOfPeriod();
        builder.click(startDateAddWork)
                .sendKeys(Keys.LEFT)
                .sendKeys(Keys.LEFT)
                .sendKeys(startPeriod)
                .build()
                .perform();
        builder.click(endDateAddWork)
                .sendKeys(Keys.LEFT)
                .sendKeys(Keys.LEFT)
                .sendKeys(endPeriod)
                .moveToElement(endDateAddWork, 100, 100)
                .click()
                .build()
                .perform();
        checkDateOfPeriod();
    }

    @DisplayName("Проверить даты периода выполнения работ по ведомости")
    public void checkDateOfPeriod() {
        driver.navigate().refresh();
        waitForElementNotPresented(loader);
        driver.findElement(xpath(estimateTitleList + "[text()='"+estimateTitle+"']")).click();
        startPeriod = String.format("%te ", dateStartPeriod) + String.format("%tB ", dateStartPeriod) + String.format("%tY", dateStartPeriod);
        endPeriod = String.format("%te ", dateEndPeriod) + String.format("%tB ", dateEndPeriod) + String.format("%tY", dateEndPeriod);
        System.out.println(startPeriod + " - " + endPeriod);
        Assertions.assertEquals(startPeriod + " - " + endPeriod, driver.findElement(xpath(estimateTitleList + "[text()='"+estimateTitle+"']/../.." +estimateDateOfPeriod)).getText());
        startPeriod = String.format("%tY-", dateStartPeriod) + String.format("%tm-", dateStartPeriod) + String.format("%td", dateStartPeriod);
        endPeriod = String.format("%tY-", dateEndPeriod) + String.format("%tm-", dateEndPeriod) + String.format("%td", dateEndPeriod);
        //viewSystemMessagePeriod();
    }

    @DisplayName("Задать даты периода ведомости работ")
    public void getDateOfPeriod() {
        dateStartPeriod = new Date();
        dateEndPeriod=new Date(dateStartPeriod.getTime()+2592000000L);
        startPeriod = String.format("%td", dateStartPeriod) + String.format("%tm", dateStartPeriod) + String.format("%tY", dateStartPeriod);
        endPeriod = String.format("%td", dateEndPeriod) + String.format("%tm", dateEndPeriod) + String.format("%tY", dateEndPeriod);
        System.out.println(startPeriod);
        System.out.println(endPeriod);
    }

    @DisplayName("Проверяю системное сообщение об установлении периода выполнения работы")
    public void viewSystemMessagePeriod(){
        selectRandomWork();
        startPeriod = String.format("%td/", dateStartPeriod) + String.format("%tm/", dateStartPeriod) + String.format("%tY", dateStartPeriod);
        endPeriod = String.format("%td/", dateEndPeriod) + String.format("%tm/", dateEndPeriod) + String.format("%tY", dateEndPeriod);
        System.out.println(startPeriod);
        System.out.println(endPeriod);
        //Assertions.assertEquals(1, driver.findElements(xpath("//p[contains(.,'Добавлен период для работы ')]")).size());
        //Assertions.assertEquals(1, driver.findElements(xpath("//p[contains(.,'с "+ startPeriod +" по " + endPeriod + "')]")).size());
    }

    @DisplayName("Открыть последнюю используемую работу")
    public void gotoLastWork() {
        driver.findElement(xpath("//p[text()='"+estimateTitle+"']")).click();
        waitForElementNotPresented(loader);
        driver.findElement(xpath("//p[text()='"+workTitle+"']")).click();
        waitForElementNotPresented(loader);
    }

    @DisplayName("Посмотреть список персонала")
    public void listOfPersonal() {
        waitForElementNotPresented(loader);
        builder.moveToElement(driver.findElement(xpath("//ul"))).click(driver.findElement(xpath("//p[text()='Контрагенты']"))).
                moveByOffset(1000,0).build().perform();
        waitForElementNotPresented(loader);
        builder.moveToElement(driver.findElement(xpath("//ul"))).click(driver.findElement(xpath("//*[text()='Сотрудники']"))).build().perform();
    }

    @DisplayName("Выбрать сотрудника")
    public void selectEmployee() {
        listOfPersonal();
        responsible = (int) (Math.random()*driver.findElements(xpath(employeeCard)).size());

        driver.findElements(xpath(employeeCard)).get(responsible).click();
        employee = driver.findElement(xpath("//*[contains(@class, 'sidePanelNameEmployee')]")).getText();
    }


    @DisplayName("Назначить ответственного на ведомость")
    public void appointResponsibleToEstimate() {
        waitForElementNotPresented(loader);
        builder.click(selectResponsibleToEstimate).sendKeys(employee).sendKeys(Keys.ENTER).build().perform();
        additionalWorksTab.click();
        waitForElementNotPresented(loader);
        driver.navigate().refresh();
        waitForElementNotPresented(loader);
        driver.findElement(xpath(estimateTitleList + "[text()='" + estimateTitle + "']")).click();
        waitForElementNotPresented(loader);
        Assertions.assertEquals(employee, driver.findElement(xpath(employeeValue)).getText());
    }

    @DisplayName("Назначить ответственного на работу")
    public void appointResponsibleToWork() {
        builder.click(selectResponsibleToEstimate).sendKeys(employee).sendKeys(Keys.ENTER).build().perform();
        driver.findElement(xpath(workTitleInDetailed)).click();
        waitForElementNotPresented(loader);
        driver.navigate().refresh();
        waitForElementNotPresented(loader);
        Assertions.assertEquals(employee, driver.findElement(xpath(employeeValue)).getText());
    }
}

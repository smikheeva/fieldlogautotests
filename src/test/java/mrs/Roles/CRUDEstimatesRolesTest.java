package mrs.Roles;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageCRUDEstimateAndWork;
import mrs.Pages.PageLogin;
import mrs.Pages.PageObject;
import mrs.Pages.PageWorkProgress;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CRUDEstimatesRolesTest extends BaseTest {

  private String estimateRoleName = "Estimate for roles";
  private String mainWorkRoleName = "Main work for roles";
  private String addWorkRoleName  = "Additional work for roles";

  @DisplayName("Создание базу для тестирования")
  @Test
  @Order(1)
  void addBaseForTestingCRUDEstimates () throws Exception {
    var work = new PageWorkProgress();
    var estimate = new PageCRUDEstimateAndWork();

    work.openProgressWork(idProject);
    estimate.createEstimateForRoles(estimateRoleName);
    estimate.selectEstimateByName(estimateRoleName);
    estimate.openMainWorkList();
    estimate.createWork(mainWorkRoleName);
    estimate.openAdditionalWorkList();
    estimate.createWork(addWorkRoleName);
  }

  @DisplayName("Роль Руководитель СК")
  @Test
  void roleHeadCQCCRUDEstimates () {
    var user = new PageLogin();
    var work = new PageWorkProgress();
    var estimate = new PageCRUDEstimateAndWork();
    var project = new PageObject();

    user.logout();
    user.loginAsHeadCQCT();

    project.openPageProject(idProject, nameProject);
    work.openProgressWork(idProject);
    estimate.checkRightViewingEstimates(estimateRoleName);
    estimate.checkNoRightForEstimate(estimateRoleName);
    estimate.checkRightViewingMainWork(mainWorkRoleName);
    estimate.checkNoRightForMainWork(mainWorkRoleName);
    estimate.backToListOfEstimates(estimateRoleName);
    estimate.checkRightViewingAddWork(addWorkRoleName);
    estimate.checkNoRightForAdditionalWork(addWorkRoleName);
  }

  @DisplayName("Роль Инженер ПТО")
  @Test
  void roleEngineerPTDCRUDEstimates() throws Exception {
    var user = new PageLogin();
    var work = new PageWorkProgress();
    var estimate = new PageCRUDEstimateAndWork();
    var project = new PageObject();
    String additionalWorkName = "AddWork" + generateString(5);
    String estimateName = "TestE" + generateString(5);

    user.logout();
    user.loginAsEngineerPTD();

    project.openPageProject(idProject, nameProject);
    work.openProgressWork(idProject);
    estimate.createEstimateForRoles(estimateName);
    estimate.selectEstimateByName(estimateName);
    estimate.updateEstimate();
    estimate.createMainWork();
    estimate.selectWorkByName(estimate.createdWorkName);
    estimate.updateWorkWithoutMeasure();
    estimate.backToListOfEstimates(estimate.editedEstimateName);

    estimate.openAdditionalWorkList();
    estimate.createWork(additionalWorkName);
    estimate.selectWorkByName(additionalWorkName);
    estimate.updateWorkWithoutMeasure();

    estimate.deleteAddWork(additionalWorkName);
    estimate.deleteEstimate(estimate.editedEstimateName);
  }

  @DisplayName("Роль Инженер СК")
  @Test
  void roleEngineerCQCCRUDEstimates () throws Exception {
    var user = new PageLogin();
    var work = new PageWorkProgress();
    var estimate = new PageCRUDEstimateAndWork();
    var project = new PageObject();
    String additionalWorkName = "AddWork" + generateString(5);

    user.logout();
    user.loginAsEngineerCQC();

    project.openPageProject(idProject, nameProject);
    work.openProgressWork(idProject);
    estimate.checkRightViewingEstimates(estimateRoleName);
    estimate.checkNoRightForEstimate(estimateRoleName);

    estimate.createMainWork();
    estimate.selectWorkByName(estimate.createdWorkName);
    estimate.updateWorkWithoutMeasure();
    estimate.backToListOfEstimates(estimateRoleName);

    estimate.openAdditionalWorkList();
    estimate.createWork(additionalWorkName);
    estimate.selectWorkByName(additionalWorkName);
    estimate.updateWorkWithoutMeasure();
    estimate.deleteWork(estimate.editedWorkName);
  }

  @DisplayName("Роль Прораб")
  @Test
  void roleForemanProject () throws Exception {
    var user = new PageLogin();
    var work = new PageWorkProgress();
    var estimate = new PageCRUDEstimateAndWork();
    var project = new PageObject();
    String additionalWorkName = generateString(5) + "AddWork";

    user.logout();
    user.loginAsForeman();

    project.openPageProject(idProject, nameProject);
    work.openProgressWork(idProject);
    estimate.checkRightViewingEstimates(estimateRoleName);
    estimate.checkNoRightForEstimate(estimateRoleName);
    estimate.checkNoRightForMainWork(mainWorkRoleName);

    estimate.backToListOfEstimates(estimateRoleName);
    estimate.openAdditionalWorkList();
    estimate.createWork(additionalWorkName);
    estimate.selectWorkByName(additionalWorkName);
    estimate.updateWorkWithoutMeasure();
    estimate.deleteWork(additionalWorkName);
  }

  @DisplayName("Роль Исполнитель")
  @Test
  void roleIssueAssigneeCRUDEstimates () throws Exception {
    var user = new PageLogin();
    var work = new PageWorkProgress();
    var estimate = new PageCRUDEstimateAndWork();
    var project = new PageObject();
    String additionalWorkName = "AddWork" + generateString(5);

    user.logout();
    user.loginAsIA();

    project.openPageProject(idProject, nameProject);
    work.openProgressWork(idProject);
    estimate.checkRightViewingEstimates(estimateRoleName);
    estimate.checkNoRightForEstimate(estimateRoleName);
    estimate.checkNoRightForMainWork(mainWorkRoleName);

    estimate.backToListOfEstimates(estimateRoleName);
    estimate.openAdditionalWorkList();
    estimate.createWork(additionalWorkName);
    estimate.selectWorkByName(additionalWorkName);
    estimate.updateWorkWithoutMeasure();
    estimate.deleteWork(additionalWorkName);
  }



}

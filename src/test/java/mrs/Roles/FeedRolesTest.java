package mrs.Roles;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.*;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileNotFoundException;

import static org.springframework.util.ResourceUtils.getFile;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FeedRolesTest extends BaseTest {

    private File estimateLink1 = getFile("Шкаф IP54.arp");

    public FeedRolesTest() throws FileNotFoundException {
    }

    @DisplayName("Создание базы для тестирования")
    @Test
    @Order(1)
    public void baseForTestingRoleFeed() throws Exception {
        startTest();
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink1);
        psd.importEstimate();
    }

    @DisplayName("Админ")
    @Test
    public void roleAdminFeed() throws Exception {
        var login = new PageLogin();
        var feed = new PageFeed();
        var work = new PageWorkProgress();
        login.logout();
        login.loginAsAdmin();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        feed.sendTextComment();
        feed.editComment();
        feed.deleteComment();
    }

    @DisplayName("Руководитель СК")
    @Test
    public void roleHeadCQCFeed() throws Exception {
        var login = new PageLogin();
        var work = new PageWorkProgress();
        var feed = new PageFeed();
        login.logout();
        login.loginAsHeadCQCT();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        feed.sendTextComment();
        feed.editComment();
        feed.deleteComment();
    }

    @DisplayName("Инженер ПТО")
    @Test
    public void roleEPTDFeed() throws Exception {
        var login = new PageLogin();
        var work = new PageWorkProgress();
        var feed = new PageFeed();
        login.logout();
        login.loginAsEngineerPTD();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        feed.sendTextComment();
        feed.editComment();
        feed.deleteComment();
    }

    @DisplayName("Инженер СК")
    @Test
    public void roleECQCFeed() throws Exception {
        var login = new PageLogin();
        var work = new PageWorkProgress();
        var feed = new PageFeed();
        login.logout();
        login.loginAsEngineerCQC();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        feed.sendTextComment();
        feed.editComment();
        feed.deleteComment();
    }

    @DisplayName("Прораб")
    @Test
    public void roleForemanFeed() throws Exception {
        var login = new PageLogin();
        var work = new PageWorkProgress();
        var feed = new PageFeed();
        login.logout();
        login.loginAsForeman();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        feed.sendTextComment();
        feed.editComment();
        feed.deleteComment();
    }

    @DisplayName("Исполнитель замечаний")
    @Test
    public void roleIAFeed() throws Exception {
        var login = new PageLogin();
        var work = new PageWorkProgress();
        var feed = new PageFeed();
        login.logout();
        login.loginAsIA();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        feed.sendTextComment();
        feed.editComment();
        feed.deleteComment();
    }
}

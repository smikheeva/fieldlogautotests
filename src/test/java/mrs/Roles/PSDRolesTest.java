package mrs.Roles;

import static org.springframework.util.ResourceUtils.getFile;

import java.io.File;
import java.io.FileNotFoundException;
import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageLogin;
import mrs.Pages.PageProject;
import mrs.Pages.PagePsd;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PSDRolesTest extends BaseTest {

    private File estimateLink1 = getFile("Шкаф IP54.arp");
    private File estimateLink2 = getFile("Здание автомойки.arp");

    public PSDRolesTest() throws FileNotFoundException {
    }

    @DisplayName("Создание базы для тестирования")
    @Test
    @Order(1)
    public void baseForTestingRolePSD() throws InterruptedException {
        startTest();
    }

    @DisplayName("Админ")
    @Test
    public void roleAdminPSD() {
        var login = new PageLogin();
        var psd = new PagePsd();
        login.logout();
        login.loginAsAdmin();
        psd.openPSD(idProject);
        System.out.println(nameProject);
        psd.createNewFolder();
        psd.editFolder();
        psd.deleteFolder();
        psd.createNewFolder();
        psd.addFolderToFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink1);
        psd.deleteFile();
        psd.selectFolder();
        psd.loadFile(estimateLink1);
        psd.importEstimate();
        psd.openPSD(idProject);
        psd.deleteFolder();
    }

    @DisplayName("Руководитель СК")
    @Test
    public void roleHeadCQCPSD() {
        var login = new PageLogin();
        var project = new PageProject();
        var psd = new PagePsd();
        login.logout();
        login.loginAsHeadCQCT();
        project.openProjectPage(idProject);
        project.selectProject(nameProject);
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.editFolder();
        psd.deleteFolder();
        psd.createNewFolder();
        psd.addFolderToFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink2);
        psd.deleteFile();
        psd.selectFolder();
        psd.loadFile(estimateLink2);
        psd.importEstimate();
    }

    @DisplayName("Инженер ПТО")
    @Test
    public void projectEPTD() {
        var login = new PageLogin();
        var project = new PageProject();
        var psd = new PagePsd();
        login.logout();
        login.loginAsEngineerPTD();
        project.openProjectPage(idProject);
        project.selectProject(nameProject);
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.editFolder();
        psd.deleteFolder();
        psd.createNewFolder();
        psd.addFolderToFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink2);
        psd.deleteFile();
        psd.selectFolder();
        psd.loadFile(estimateLink2);
        psd.importEstimate();
    }

    @DisplayName("Инженер СК")
    @Test
    public void projectECQC() {
        var login = new PageLogin();
        var project = new PageProject();
        var psd = new PagePsd();
        login.logout();
        login.loginAsEngineerCQC();
        project.openProjectPage(idProject);
        project.selectProject(nameProject);
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.editFolder();
        psd.deleteFolder();
        psd.createNewFolder();
        psd.addFolderToFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink2);
        psd.deleteFile();
        psd.selectFolder();
        psd.loadFile(estimateLink2);
        psd.importEstimate();
    }

    @DisplayName("Прораб")
    @Test
    public void projectForeman() {
        var login = new PageLogin();
        var project = new PageProject();
        var psd = new PagePsd();
        login.logout();
        login.loginAsForeman();
        project.openProjectPage(idProject);
        project.selectProject(nameProject);
        psd.openPSD(idProject);
        psd.psdDisable();
    }

    @DisplayName("Исполнитель замечаний")
    @Test
    public void projectIA() {
        var login = new PageLogin();
        var project = new PageProject();
        var psd = new PagePsd();
        login.logout();
        login.loginAsIA();
        project.openProjectPage(idProject);
        project.selectProject(nameProject);
        psd.openPSD(idProject);
        psd.psdDisable();
    }
}

package mrs.Roles;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageLogin;
import mrs.Pages.PageNotebook;
import mrs.Pages.PagePsd;
import mrs.Pages.PageWorkProgress;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileNotFoundException;

import static org.springframework.util.ResourceUtils.getFile;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProgressWorkRolesTest extends BaseTest {
    private File estimateLink1 = getFile("Шкаф IP54.arp");
    private File estimateLink2 = getFile("Здание автомойки.arp");
    private File estimateLink3 = getFile("Котельная 2,6 МВТ д. Борисово (Лента).arp");

    public ProgressWorkRolesTest() throws FileNotFoundException {
    }

    @DisplayName("Создание базы для тестирования")
    @Test
    @Order(1)
    void baseForTestingRoleProgressWork() throws Exception {
        startTest();
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink1);
        psd.loadFile(estimateLink2);
        psd.loadFile(estimateLink3);
        psd.importSomeEstimate(estimateLink1, estimateLink2, estimateLink3);
    }

    @DisplayName("Админ")
    @Test
    void roleAdminProgressWork() throws Exception {
        var login = new PageLogin();
        var work = new PageWorkProgress();
        var note = new PageNotebook();
        login.logout();
        login.loginAsAdmin();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addWorkRecord();
        work.confirmThisRecordButtonClick();
        work.checkPresenceWorkInNotebook();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addWorkRecord();
        work.updateWorkRecord();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.returnToListOfEstimates();
        note.openNotebook(idProject);
        note.confirmWork();
    }

    @DisplayName("Руководитель СК")
    @Test
    void roleHeadCQCProgressWork() throws Exception {
        var login = new PageLogin();
        var work = new PageWorkProgress();
        var note = new PageNotebook();
        login.logout();
        login.loginAsHeadCQCT();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addWorkRecordDisable();
        work.returnToListOfEstimates();
        note.openNotebook(idProject);
        note.confirmWorkDisable();
    }

    @DisplayName("Инженер ПТО")
    @Test
    void roleEPTDProgressWork() throws Exception {
        var login = new PageLogin();
        var work = new PageWorkProgress();
        var note = new PageNotebook();
        login.logout();
        login.loginAsEngineerPTD();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addWorkRecordDisable();
        work.returnToListOfEstimates();
        note.openNotebook(idProject);
        note.confirmWorkDisable();
    }

    @DisplayName("Инженер СК")
    @Test
    void roleECQCProgressWork() throws Exception {
        var login = new PageLogin();
        var work = new PageWorkProgress();
        var note = new PageNotebook();
        login.logout();
        login.loginAsEngineerCQC();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addWorkRecordDisable();
        work.returnToListOfEstimates();
        note.openNotebook(idProject);
        note.confirmWorkDisable();
    }

    @DisplayName("Прораб")
    @Test
    void roleForemanProgressWork() throws Exception {
        var login = new PageLogin();
        var work = new PageWorkProgress();
        var note = new PageNotebook();
        login.logout();
        login.loginAsForeman();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.selectRandomEstimateAndWork();
        work.addWorkRecord();
        work.confirmThisRecordButtonClick();
        work.checkPresenceWorkInNotebook();
        note.confirmWork();
    }

    @DisplayName("Исполнитель замечаний")
    @Test
    void roleIAProgressWork() throws Exception {
        var login = new PageLogin();
        var work = new PageWorkProgress();
        var note = new PageNotebook();
        login.logout();
        login.loginAsIA();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addWorkRecord();
        work.confirmThisRecordButtonClick();
        work.checkPresenceWorkInNotebook();
        note.confirmWorkDisable();
    }
}

package mrs.Roles;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageAreas;
import mrs.Pages.PageLogin;
import mrs.Pages.PageProject;
import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProjectRolesTest extends BaseTest {

    @DisplayName("Создание базы для тестирования")
    @Test
    @Order(1)
    public void baseForTestingRoleProject() throws Exception {
        startTest();
        var project = new PageProject();
        project.openProjectPage(idProject);
    }

    @DisplayName("Admin")
    @Test
    public void roleAdminProject() {
        var login = new PageLogin();
        var project = new PageProject();
        var area = new PageAreas();
        login.logout();
        login.loginAsAdmin();
        project.openProjectPage(idProject);
        project.createProject();
        project.saveEditProjectAction();
        project.deleteProject();
        area.openAreasSidebar(idProject);
        area.createObject();
        area.checkObjectInfo();
        area.updateObject();
        area.checkObjectInfo();
        area.createAreasInObject();
        area.updateArea();
        area.deleteArea();
        area.deleteObject();
    }

    @DisplayName("Руководитель СК")
    @Test
    public void roleHCQCProject() {
        var login = new PageLogin();
        var project = new PageProject();
        var area = new PageAreas();
        login.logout();
        login.loginAsHeadCQCT();
        project.openProjectPage(idProject);
        project.createProject();
        project.saveEditProjectAction();
        project.deleteProject();
        area.openAreasSidebar(idProject);
        area.createObject();
        area.updateObject();
        area.createAreasInObject();
        area.updateArea();
        area.deleteArea();
        area.deleteObject();
    }

    @DisplayName("Инженер ПТО")
    @Test
    public void roleEPTDProject() {
        var login = new PageLogin();
        var project = new PageProject();
        var area = new PageAreas();
        login.logout();
        login.loginAsEngineerPTD();
        project.openProjectPage(idProject);
        project.createProjectDisable();
        area.openAreasSidebar(idProject);
        area.createObject();
        area.updateObject();
        area.createAreasInObject();
        area.updateArea();
        area.deleteArea();
        area.deleteObject();
    }

    @DisplayName("Инженер СК")
    @Test
    public void roleECQCProject() {
        var login = new PageLogin();
        var project = new PageProject();
        var area = new PageAreas();
        login.logout();
        login.loginAsEngineerCQC();
        project.openProjectPage(idProject);
        project.createProjectDisable();
        area.openAreasSidebar(idProject);
        area.createObject();
        area.updateObject();
        area.createAreasInObject();
        area.updateArea();
        area.deleteArea();
        area.deleteObject();
    }

    @DisplayName("Прораб")
    @Test
    public void roleForemanProject() {
        var login = new PageLogin();
        var project = new PageProject();
        var area = new PageAreas();
        login.logout();
        login.loginAsForeman();
        project.openProjectPage(idProject);
        project.createProjectDisable();
        area.openAreasSidebar(idProject);
        area.createObjectDisable();
    }

    @DisplayName("Исполнитель замечаний")
    @Test
    public void roleIAProject() {
        var login = new PageLogin();
        var project = new PageProject();
        var area = new PageAreas();
        login.logout();
        login.loginAsIA();
        project.openProjectPage(idProject);
        project.createProjectDisable();
        area.openAreasSidebar(idProject);
        area.createObjectDisable();
    }
}

package mrs.Tests;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageAreas;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class AreasTest extends BaseTest {

  @DisplayName("Создать новый объект")
  @Test
  public void CreateNewObject() {
    var area = new PageAreas();

    area.openAreasSidebar(idProject);
    area.createObject();
  }

  @DisplayName("Отменить создание нового объекта")
  @Test
  public void CancelCreateObject() throws InterruptedException {
    var area = new PageAreas();

    startTest();
    area.openAreasSidebar(idProject);
    area.cancelCreateObject();
  }

  @DisplayName("Создать новый объект и несколько участков внутри объекта")
  @Test
  public void CreateObjectWithAreas() throws InterruptedException {
    var area = new PageAreas();

    startTest();
    area.openAreasSidebar(idProject);
    area.createObject();
    area.createAreasInObject();
  }

  @DisplayName("Создать новый объект. Создать несколько уровней вложенности участков")
  @Test
  public void CreateSeveralLevelsOfAreas() throws InterruptedException {
    var area = new PageAreas();

    startTest();
    area.openAreasSidebar(idProject);
    area.createObject();
    area.createAreaIntoArea(3);
    area.checkAreasLevels(3);
  }

  @DisplayName("Редактировать объект")
  @Test
  public void UpdateObject() throws InterruptedException {
    var area = new PageAreas();

    startTest();
    area.openAreasSidebar(idProject);
    area.createObject();
    area.checkObjectInfo();
    area.updateObject();
    area.checkObjectInfo();
  }

  @DisplayName("Редактировать участок")
  @Test
  public void UpdateAreas() throws InterruptedException {
    var area = new PageAreas();

    startTest();
    area.openAreasSidebar(idProject);
    area.createObject();
    area.createAreaInObject();
    area.updateArea();
  }

  @DisplayName("Удалить участок")
  @Test
  public void DeleteArea() throws InterruptedException {
    var area = new PageAreas();

    startTest();
    area.openAreasSidebar(idProject);
    area.createObject();
    area.createAreaInObject();
    area.deleteArea();
  }

  @DisplayName("Удалить объект")
  @Test
  public void DeleteObject() throws InterruptedException {
    var area = new PageAreas();

    startTest();
    area.openAreasSidebar(idProject);
    area.createObject();
    area.deleteObject();
  }
}

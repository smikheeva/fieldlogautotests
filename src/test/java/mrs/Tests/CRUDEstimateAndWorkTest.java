package mrs.Tests;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageCRUDEstimateAndWork;
import mrs.Pages.PageNotebook;
import mrs.Pages.PagePsd;
import mrs.Pages.PageWorkProgress;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.io.File;
import java.io.FileNotFoundException;

import static org.springframework.util.ResourceUtils.getFile;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CRUDEstimateAndWorkTest extends BaseTest {

    private File estimateToUpdate   = getFile("Шкаф IP54.arp");
    private File mainEstimate       = getFile("Здание автомойки.arp");
    private File estimateToDelete   = getFile("Котельная 2,6 МВТ д. Борисово (Лента).arp");

    private String importedWork2 = "Устройства промежуточные на количество лучей: 1*(УКШ-1)";
    private String importedWork3 = "Звуковой оповещатель(Маяк-24КП, CWSO-RR-W1)";

    public CRUDEstimateAndWorkTest() throws FileNotFoundException {
    }

    @DisplayName("Создание сметы и работы в смете")
    @Test
    void createMainWorkInEstimate () throws Exception {
        var work = new PageWorkProgress();
        var estimate = new PageCRUDEstimateAndWork();

        startTest();
        work.openProgressWork(idProject);
        estimate.createEstimate();
        estimate.selectEstimateByName(estimate.createdEstimateName);
        estimate.openMainWorkList();
        estimate.createMainWork();
        estimate.createAddWork();
    }

    @DisplayName("Отменить создание ведомости работ")
    @Test
    void cancelCreateEstimate() throws InterruptedException {
        var work = new PageWorkProgress();
        var estimate = new PageCRUDEstimateAndWork();

        startTest();
        work.openProgressWork(idProject);
        estimate.cancelCreateEstimate();
    }

    @DisplayName("Отменить создание работы")
    @Test
    void cancelCreateWork() throws Exception {
        var work = new PageWorkProgress();
        var estimate = new PageCRUDEstimateAndWork();

        startTest();
        work.openProgressWork(idProject);

        estimate.createEstimate();
        estimate.selectEstimateByName(estimate.createdEstimateName);
        work.selectRandomEstimate();
        estimate.cancelCreateMainWork();
    }

    @DisplayName("Редактировать созданную ведомость работ")
    @Test
    void editCreatedEstimate() throws InterruptedException {
        var work = new PageWorkProgress();
        var estimate = new PageCRUDEstimateAndWork();

        startTest();
        work.openProgressWork(idProject);
        estimate.createEstimate();
        estimate.selectEstimateByName(estimate.createdEstimateName);
        estimate.updateEstimate();
    }

    @DisplayName("Отменить редактирование ведомости работ")
    @Test
    void cancelEditEstimate() throws InterruptedException {
        var work = new PageWorkProgress();
        var estimate = new PageCRUDEstimateAndWork();

        startTest();
        work.openProgressWork(idProject);
        estimate.createEstimate();
        estimate.selectEstimateByName(estimate.createdEstimateName);
        estimate.cancelUpdateEstimate();
    }

    @DisplayName("Удалить ведомость работ")
    @Test
    void deleteEstimate() throws InterruptedException {
        var work = new PageWorkProgress();
        var estimate = new PageCRUDEstimateAndWork();

        startTest();
        work.openProgressWork(idProject);
        estimate.createEstimate();
        estimate.selectEstimateByName(estimate.createdEstimateName);
        estimate.deleteEstimate(estimate.createdEstimateName);
    }

    @DisplayName("Редактировать созданную работу с единицами измерения")
    @Test
    void editCreatedWorkWithMeasure() throws Exception {
        var work = new PageWorkProgress();
        var estimate = new PageCRUDEstimateAndWork();

        startTest();
        work.openProgressWork(idProject);
        estimate.createEstimate();
        estimate.selectEstimateByName(estimate.createdEstimateName);
        estimate.createMainWork();
        estimate.selectWorkByName(estimate.createdWorkName);
        estimate.updateMainWorkWithMeasure();
    }

    @DisplayName("Удалить работу")
    @Test
    void deleteWork() throws Exception {
        var work = new PageWorkProgress();
        var estimate = new PageCRUDEstimateAndWork();

        startTest();
        work.openProgressWork(idProject);
        estimate.createEstimate();
        estimate.selectEstimateByName(estimate.createdEstimateName);
        estimate.createMainWork();
        estimate.selectWorkByName(estimate.createdWorkName);
        estimate.deleteWork(estimate.createdWorkName);
    }

    @DisplayName("Проверить изменение информации в блокноте")
    @Test
    void changeInfoInJournal() throws Exception {
        var work = new PageWorkProgress();
        var estimate = new PageCRUDEstimateAndWork();
        var note = new PageNotebook();

        startTest();
        work.openProgressWork(idProject);
        estimate.createEstimate();
        estimate.selectEstimateByName(estimate.createdEstimateName);
        estimate.createMainWork();
        estimate.selectWorkByName(estimate.createdWorkName);

        work.addWorkRecord();
        note.openNotebook(idProject);
        estimate.checkWorkNameInNotebook(estimate.createdWorkName);
        estimate.confirmWork(estimate.createdWorkName);

        work.openProgressWork(idProject);
        estimate.selectEstimateByName(estimate.createdEstimateName);
        estimate.selectWorkByName(estimate.createdWorkName);
        estimate.updateWorkWithoutMeasure();
        note.openNotebook(idProject);
        estimate.checkWorkNameInJournal(estimate.editedWorkName);
    }

    @DisplayName("CRUD импортированной ведомости")
    @Test
    void createTestEstimateList() throws Exception {
        var psd = new PagePsd();
        var work = new PageWorkProgress();
        var estimate = new PageCRUDEstimateAndWork();

        startTest();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.selectFolder();
        psd.loadSomeEstimates(estimateToUpdate, mainEstimate, estimateToDelete);
        psd.importSomeEstimate(estimateToUpdate, mainEstimate, estimateToDelete);

        System.out.println("Редактирую ведомость");
        work.openProgressWork(idProject);
        estimate.selectEstimateByName(estimateToUpdate.getName().replace(".arp", ""));
        estimate.updateEstimate();

        System.out.println("Удаляю ведомость");
        estimate.selectEstimateByName(estimateToDelete.getName().replace(".arp", ""));
        estimate.deleteEstimate(estimateToDelete.getName().replace(".arp", ""));

        System.out.println("Редактирую работу");
        estimate.selectEstimateByName(mainEstimate.getName().replace(".arp", ""));
        estimate.createMainWork();
        estimate.selectWorkByName(importedWork3);
        estimate.updateWorkWithoutMeasure();

        System.out.println("Удаляю работу");
        work.returnToListOfEstimates();
        estimate.selectEstimateByName(mainEstimate.getName().replace(".arp", ""));
        estimate.selectWorkByName(importedWork2);
        estimate.deleteWork(importedWork2);
    }
}

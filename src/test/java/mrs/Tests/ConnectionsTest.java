package mrs.Tests;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.UUID;

import static org.springframework.util.ResourceUtils.getFile;

public class ConnectionsTest extends BaseTest {

    private File estimateLink1 = getFile("Шкаф IP54.arp");
    private File estimateLink2 = getFile("Здание автомойки.arp");
    UUID idProject1;
    UUID idProject2;

    public ConnectionsTest() throws FileNotFoundException {
    }

    @DisplayName("Проверяю связи в PSD")
    @Test
    public void checkConnectionsPSD() throws Exception {
        startTest();
        var con = new PageConnections();
        var psd = new PagePsd();
        idProject1 = createProjectInDto();
        idProject2 = createProjectInDto();
        psd.openPSD(idProject1);
        psd.createNewFolder();
        psd.openPSD(idProject2);
        psd.createNewFolder();
        con.getFolderInPSD();
        openPageProjectINDto(idProject1);
        psd.openPSD(idProject1);
        con.checkFolder();
        deleteProjectInDto(idProject1);
        deleteProjectInDto(idProject2);
    }

    @DisplayName("Проверяю связив в ведомостей работ")
    @Test
    public void checkConnectionsEstimates() throws Exception {
        startTest();
        var con = new PageConnections();
        var work = new PageWorkProgress();
        var crude = new PageCRUDEstimateAndWork();
        idProject1 = createProjectInDto();
        work.openProgressWork(idProject1);
        crude.createEstimate();
        work.selectRandomEstimate();
        crude.createMainWork();
        idProject2 = createProjectInDto();
        work.openProgressWork(idProject2);
        crude.createEstimate();
        work.selectRandomEstimate();
        crude.createMainWork();
        con.getEstimate();
        openPageProjectINDto(idProject1);
        work.openProgressWork(idProject1);
        con.checkEstimate();
        deleteProjectInDto(idProject1);
        deleteProjectInDto(idProject2);
    }

    @DisplayName("Проверяю связи в журнале")
    @Test
    public void checkConnectionsNotebook() throws Exception {
        startTest();
        var con = new PageConnections();
        var work = new PageWorkProgress();
        var note = new PageNotebook();
        var crude = new PageCRUDEstimateAndWork();
        idProject1 = createProjectInDto();
        work.openProgressWork(idProject1);
        crude.createEstimate();
        work.selectRandomEstimate();
        crude.createMainWork();
        con.getEstimate();
        idProject2 = createProjectInDto();
        work.openProgressWork(idProject2);
        crude.createEstimate();
        work.selectRandomEstimate();
        crude.createMainWork();
        work.selectRandomEstimate();
        work.selectRandomWork();
        work.addWorkRecord();
        note.openNotebook(idProject2);
        work.checkPresenceWorkInNotebook();
        openPageProjectINDto(idProject1);
        note.openNotebook(idProject1);
        con.checkWorkInNotebook();
        note.openNotebook(idProject2);
        note.confirmWork();
        note.openNotebook(idProject1);
        con.checkWorkInJournal();
        deleteProjectInDto(idProject1);
        deleteProjectInDto(idProject2);
    }

    @DisplayName("Проверяю связи в участках")
    @Test
    public void checkConnectionsAreas() throws Exception {
        startTest();
        var con = new PageConnections();
        var area = new PageAreas();
        idProject1 = createProjectInDto();
        area.openAreasSidebar(idProject1);
        area.createObject();
        idProject2 = createProjectInDto();
        area.openAreasSidebar(idProject2);
        area.createObject();
        openPageProjectINDto(idProject1);
        area.openAreasSidebar(idProject1);
        con.checkArea();
        deleteProjectInDto(idProject1);
        deleteProjectInDto(idProject2);
    }
}

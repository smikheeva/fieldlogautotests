package mrs.Tests;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageCRUDEstimateAndWork;
import mrs.Pages.PageDocumentation;
import mrs.Pages.PageNotebook;
import mrs.Pages.PageWorkProgress;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

public class DocumentationTest extends BaseTest {

    public DocumentationTest(){
    }

    @DisplayName("Скачать CSV")
    @Test
    @Order(1)
    public void downloadCSV() throws Exception {
        var doc = new PageDocumentation();
        var work = new PageWorkProgress();
        var note = new PageNotebook();
        var crude = new PageCRUDEstimateAndWork();
        startTest();
        work.openProgressWork(idProject);
        crude.createEstimate();
        work.selectRandomEstimate();
        crude.createMainWork();
        work.selectRandomEstimate();
        work.selectRandomWork();
        work.addWorkRecord();
        note.openNotebook(idProject);
        note.confirmWork();
        doc.openDocumentation(idProject);
        doc.downloadDocumentation();
    }
}

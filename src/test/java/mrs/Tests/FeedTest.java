package mrs.Tests;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageCRUDEstimateAndWork;
import mrs.Pages.PageFeed;
import mrs.Pages.PageWorkProgress;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileNotFoundException;

import static org.springframework.util.ResourceUtils.getFile;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FeedTest extends BaseTest {

    private File estimateLink1 = getFile("Шкаф IP54.arp");

    public FeedTest() throws FileNotFoundException {
    }

    @DisplayName("Отправить текстовый комментарий")
    @Test
    @Order(1)
    public void SendTextComment() throws Exception {
        startTest();
        var feed = new PageFeed();
        var work = new PageWorkProgress();
        var crude = new PageCRUDEstimateAndWork();
        startTest();
        work.openProgressWork(idProject);
        crude.createEstimate();
        work.selectRandomEstimate();
        crude.createMainWork();
        work.selectRandomWork();
        feed.sendTextComment();
    }

    @DisplayName("Редактировать текстовый комментарий")
    @Test
    public void EditTextComment() throws Exception {
        var feed = new PageFeed();
        var work = new PageWorkProgress();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        feed.sendTextComment();
        feed.editComment();
    }

    @DisplayName("Отменить редактированиеь текстового комментария")
    @Test
    public void CancelEditTextComment() throws Exception {
        var feed = new PageFeed();
        var work = new PageWorkProgress();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        feed.sendTextComment();
        feed.cancelEditComment();
    }

    @DisplayName("Удалить текстовый комментарий")
    @Test
    public void DeleteTextComment() throws Exception {
        var feed = new PageFeed();
        var work = new PageWorkProgress();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        feed.sendTextComment();
        feed.deleteComment();
    }
}
package mrs.Tests;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageNavigation;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class NavigationTest extends BaseTest {

    @Test
    @DisplayName("Переход к списку проектов")
    void ListOfProjectsSidebar () {
        var page = new PageNavigation();
        page.OpenListOfProjectsSidebar();
    }

    @Nested
    @DisplayName("Навигация")
    class SidebarNavigation {

        @Test
        @DisplayName("Переход к ПСД")
        void PSDSidebar () {
            var page = new PageNavigation();
            page.OpenPSDSidebar();
        }

        @Test
        @DisplayName("Переход к Прогрессу работ")
        void EstimatesSidebar () {
            var page = new PageNavigation();
            page.OpenEstimatesSidebar();
        }

        @Test
        @DisplayName("Переход к Журналу")
        void JournalSidebar () {
            var page = new PageNavigation();
            page.OpenJournalSidebar();
        }

        @Test
        @DisplayName("Переход к ИД")
        void DocumentationSidebar () {
            var page = new PageNavigation();
            page.OpenDocumentationSidebar();
        }

        @Test
        @DisplayName("Переход к Участкам")
        void SubprojectsSidebar () {
            var page = new PageNavigation();
            page.OpenSubprojectsSidebar();
        }

        @Test
        @DisplayName("Переход к Моей странице")
        void MyPageSidebar () {
            var page = new PageNavigation();
            page.OpenMyPageSidebar();
        }

        @Test
        @DisplayName("Переход к Странице Юзера")
        void ProfileSidebar () {
            var page = new PageNavigation();
            page.OpenProfileSidebar();
        }
    }
}

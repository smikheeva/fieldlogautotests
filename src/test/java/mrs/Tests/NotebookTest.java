package mrs.Tests;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageCRUDEstimateAndWork;
import mrs.Pages.PageNotebook;
import mrs.Pages.PageWorkProgress;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileNotFoundException;

import static org.springframework.util.ResourceUtils.getFile;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class NotebookTest extends BaseTest {

    private File estimateLink = getFile("Шкаф IP54.arp");

    public NotebookTest() throws FileNotFoundException {
    }

    @DisplayName("Отмена подтверждения работ")
    @Test
    @Order(1)
    public void cancelConfirmWork() throws Exception {
        var work = new PageWorkProgress();
        var note = new PageNotebook();
        var crude = new PageCRUDEstimateAndWork();

        startTest();
        work.openProgressWork(idProject);
        crude.createEstimate();
        work.selectRandomEstimate();
        crude.createMainWork();
        crude.createMainWork();
        crude.createMainWork();
        work.selectRandomEstimateAndWork();
        work.addWorkRecord();
        note.openNotebook(idProject);
        //note.startWorkDay();
        note.cancelConfirmWork();
    }

    @DisplayName("Подтверждение всех работ")
    @Test
    public void allConfirmWork() throws Exception {
        var note = new PageNotebook();
        var work = new PageWorkProgress();

        startTest();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addWorkRecord();
        note.openNotebook(idProject);
        note.checkCountWorks();
        note.confirmWork();
        work.openProgressWork(idProject);
        //note.checkedSystemMessage();
    }

    @DisplayName("Подтверждение ранее подтвержденной работы")
    @Test
    public void confirmConfirmedWork() throws Exception {
        var work = new PageWorkProgress();
        var note = new PageNotebook();

        startTest();
        work.openProgressWork(idProject);
        work.selectRandomEstimate();
        work.selectRandomWork();
        work.addWorkRecord();
        work.selectRandomWork();
        work.addWorkRecord();
        note.openNotebook(idProject);
        note.confirmWork();
        work.openProgressWork(idProject);
        work.gotoLastWork();
        work.addWorkRecord();
        note.openNotebook(idProject);
        note.confirmWork();
    }

    @DisplayName("Клик по работе в блокноте")
    @Test
    public void clickOnWorkInNotebook() throws Exception {
        var work = new PageWorkProgress();
        var note = new PageNotebook();

        startTest();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addWorkRecord();
        note.openNotebook(idProject);
        note.clickOnWorkInNotebook();
    }
}
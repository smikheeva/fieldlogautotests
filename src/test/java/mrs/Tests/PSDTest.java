package mrs.Tests;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PagePsd;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileNotFoundException;

import static org.springframework.util.ResourceUtils.getFile;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PSDTest extends BaseTest {
    private File estimateLink1 = getFile("Шкаф IP54.arp");
    private File estimateLink2 = getFile("Здание автомойки.arp");
    private File estimateLink3 = getFile("Котельная 2,6 МВТ д. Борисово (Лента).arp");

    public PSDTest() throws FileNotFoundException {
    }

    @DisplayName("Открыть проектно-сметную документацию")
    @Test
    @Order(1)
    public void openPSD() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
    }

    @DisplayName("Создать папку")
    @Test
    public void createFolders() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
    }

    @DisplayName("Редактировать папку")
    @Test
    public void editFolder() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.editFolder();
    }

    @DisplayName("Удалить папку")
    @Test
    public void deleteFolder() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.deleteFolder();
    }

    @DisplayName("Добавить папку в папку")
    @Test
    public void addFolderToFolder() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.addFolderToFolder();
    }

    @DisplayName("Загрузить файл в папку")
    @Test
    public void loadFileToFolder() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink1);
    }

    @DisplayName("Удалить файл из папки")
    @Test
    public void deleteFileFromFolder() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink2);
        psd.deleteFile();
    }

    @DisplayName("Импортировать смету")
    @Test
    public void importEstimateFromFolder() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink3);
        psd.importEstimate();
    }

    @DisplayName("Загрузить файл в отредактированную папку")
    @Test
    public void loadFileToUpdatedFolder() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.editFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink1);
    }

    @DisplayName("Удалить файл из отредактированной папки")
    @Test
    public void deleteFileFromUpdatedFolder() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.editFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink2);
        psd.deleteFile();
    }

    @DisplayName("Импортировать смету из отредактированной папки")
    @Test
    public void importEstimateFromUpdatedFolder() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.editFolder();
        psd.selectFolder();
        psd.loadFile(estimateLink1);
        psd.importEstimate();
    }

    @DisplayName("Удалить отредактированную папку")
    @Test
    public void deleteUpdatedFolder() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.createNewFolder();
        psd.editFolder();
        psd.deleteFolder();
    }
}

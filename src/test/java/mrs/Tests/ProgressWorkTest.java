package mrs.Tests;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageCRUDEstimateAndWork;
import mrs.Pages.PageNotebook;
import mrs.Pages.PageProject;
import mrs.Pages.PageWorkProgress;
import org.junit.jupiter.api.*;

import java.io.File;
import java.io.FileNotFoundException;

import static org.springframework.util.ResourceUtils.getFile;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProgressWorkTest extends BaseTest {

    private File estimateLink1 = getFile("Шкаф IP54.arp");
    private File estimateLink2 = getFile("Здание автомойки.arp");
    private File estimateLink3 = getFile("Котельная 2,6 МВТ д. Борисово(Лента).arp");

    public ProgressWorkTest() throws FileNotFoundException {
    }

    /*@DisplayName("Загрузка тестовой сметы")
    @Test
    @Order(1)
    void createTestEstimateList() {
        var psd = new PagePsd();
        psd.openPSD(idProject);
        psd.selectFolder();
        psd.loadFile(estimateLink1);
        psd.loadFile(estimateLink2);
        psd.loadFile(estimateLink3);
        psd.importSomeEstimate(estimateLink1, estimateLink2, estimateLink3);
    }*/

    @DisplayName("Создане списка смет и работ вручную")
    @Test
    @Order(1)
    void createTestEstimateList() throws Exception {
        var work = new PageWorkProgress();
        var estimate = new PageCRUDEstimateAndWork();

        work.openProgressWork(idProject);
        estimate.createEstimate();
        estimate.selectEstimateByName(estimate.createdEstimateName);
        estimate.openMainWorkList();
        estimate.createMainWork();
        estimate.createMainWork();
        estimate.createMainWork();

        estimate.createEstimate();
        estimate.selectEstimateByName(estimate.createdEstimateName);
        estimate.openMainWorkList();
        estimate.createMainWork();
        estimate.createMainWork();
        estimate.createMainWork();
    }

    @DisplayName("Проверяю соответствие ведомостей и работ на моей странице и в прогрессе работ")//+ -  пока список работ на моей странице пустой
    @Test
    void checkEstimatesAndWorksInMyPage() throws InterruptedException {
        var work = new PageWorkProgress();

        startTest();
        work.openProgressWork(idProject);
        work.getEstimates();
        work.getWorks();
        work.openMyPage(idProject);
        work.checkEstimatesInMyPage();
    }

    @DisplayName("Клик по ведомости на моей странице")
    @Test
    void clickOnEstimateInMyPage() throws InterruptedException {
        var work = new PageWorkProgress();
        var project = new PageProject();

        startTest();
        project.openPageProject(idProject, nameProject);
        work.getEstimates();
        work.clickOnEstimateInMyPage();
    }

    /*@DisplayName("Клик по работе на моей странице")
    @Test
    void clickOnWorkInMyPage() throws InterruptedException {
        var work = new PageWorkProgress();

        startTest();
        work.openMyPage(idProject);
        work.getWorks();
        work.openMyPage(idProject);
        work.clickOnWorkInMyPage();
    }*/

    @DisplayName("Переключение между сметами и работами")
    @Test
    void switchingEstimatesAndJobs() throws Exception {
        var work = new PageWorkProgress();

        startTest();
        work.openProgressWork(idProject);
        work.switchingEstimates();
        work.switchingMainJobs();
        work.switchingAddJobs();
        work.switchingJobsTab();
    }

    @DisplayName("Установить период выполнения сметы")
    @Test
    void setEstimatePeriod() throws Exception {
        var work = new PageWorkProgress();

        startTest();
        work.openProgressWork(idProject);
        work.selectRandomEstimate();
        work.addPeriodOfEstimate();
    }

    @DisplayName("Установить период выполнения работы")
    @Test
    void setWorkPeriod() throws Exception {
        var work = new PageWorkProgress();

        startTest();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addPeriodOfWork();
    }

    @DisplayName("Отменить добавление записи о выполнении валидного объёма работы")
    @Test
    void cancelAddingRecordOfWork() throws Exception {
        var work = new PageWorkProgress();
        var note = new PageNotebook();

        startTest();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.cancelAddingWorkRecord();
        note.openNotebook(idProject);
        work.checkAbsenceWorkInNotebook();
    }

    @DisplayName("Подтвердить добавление записи о выполнении валидного объёма работы")
    @Test
    void addRecordOfWork() throws Exception {
        var work = new PageWorkProgress();

        startTest();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addWorkRecord();
        work.confirmThisRecordButtonClick();
        work.checkPresenceWorkInNotebook();
    }

    @DisplayName("Проверка поведения при вводе объема больше планового")
    @Test
    void addRecordMoreThenPlaned() throws Exception {
        var work = new PageWorkProgress();

        startTest();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addValueMoreThenPlaned();
    }

    @DisplayName("Редактирование записи о выполнении работ")
    @Test
    void updateRecordOfWork() throws Exception {
        var work = new PageWorkProgress();

        startTest();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addWorkRecord();
        work.updateWorkRecord();
    }

    @DisplayName("Отмена редактирования записи о выполнении работ")
    @Test
    void cancelUpdateRecordOfWork() throws Exception {
        var work = new PageWorkProgress();

        startTest();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.addWorkRecord();
        work.cancelUpdateWorkRecord();
    }

    @DisplayName("Проверка перехода к списку ведомостей по кнопке возврата")
    @Test
    void returnBackToEstimates() throws Exception {
        var work = new PageWorkProgress();

        startTest();
        work.openProgressWork(idProject);
        work.selectRandomEstimateAndWork();
        work.returnToListOfEstimates();
    }

    @DisplayName("Назначить ответственного на смету и на работу")
    @Test
    void appointResponsible() throws Exception {
        var work = new PageWorkProgress();

        startTest();
        work.openProgressWork(idProject);
        work.selectEmployee();
        work.openProgressWork(idProject);
        work.selectRandomEstimate();
        work.appointResponsibleToEstimate();
        work.selectRandomEstimateAndWork();
        work.appointResponsibleToWork();
    }
}


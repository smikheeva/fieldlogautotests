package mrs.Tests;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageProject;
import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProjectTest extends BaseTest {

    @DisplayName("Создать проект и отменить, проверка обязательных полей")
    @Test
    @Order(1)
    public void cancelCreatingProject() {
        var  project = new PageProject();
        project.openProjectPage(idProject);
        project.cancelCreateProject();
    }

    @DisplayName ("Создать проект и подтвердить")
    @Test
    public void confirmCreatingProject() {
        var  project = new PageProject();
        project.createProject();
    }

    @DisplayName ("Редактировать проект и отменить изменения")
    @Test
    public void cancelEditingProject() {
        var  project = new PageProject();
        project.createProject();
        project.cancelEditProjectAction();
    }

    @DisplayName ("Редактировать проект и сохранить изменения")
    @Test
    public void saveEditingProject() {
        var  project = new PageProject();
        project.createProject();
        project.saveEditProjectAction();
    }

    @DisplayName ("Удалить проект")
    @Test
    public void deleteProject() {
        var project = new PageProject();
        project.createProject();
        project.deleteProject();
        project.checkActiveProject();
    }

    /*@DisplayName ("Добавить участника и отменить")
    @Test
    public void cancelAddingParticipants() throws Exception {
        var  project = new PageProject();
        project.createProject();
        project.addParticipantsAction();
        project.fillingAddParticipantToProject();
        project.cancelAddParticipantToProject();
    }

    @DisplayName ("Добавить участника и подтвердить")
    @Test
    public void confirmAddingParticipants() throws Exception {
        var  project = new PageProject();
        project.createProject();
        project.addParticipantsAction();
        project.fillingAddParticipantToProject();
        project.confirmAddParticipantToProject();
    }

    @DisplayName ("Выбрать некоторых сотрудников для добавления и отмеить")
    @Test
    public void cancelChoosingEmployeeForAdding() throws Exception {
        var  project = new PageProject();
        project.createProject();
        project.addParticipantsAction();
        project.fillingAddParticipantToProject();
        project.confirmAddParticipantToProject();
        project.cancelAddEmployee();
    }

    @DisplayName ("Выбрать некоторых сотрудников для добавления и подтвердить")
    @Test
    public void confirmChoosingEmployeeForAdding() throws Exception {
        var  project = new PageProject();
        project.createProject();
        project.addParticipantsAction();
        project.fillingAddParticipantToProject();
        project.confirmAddParticipantToProject();
        project.confirmAddEmployee();
    }

    @DisplayName ("Добавить участника и подтвердить, выбрать всех сотрудников для добавления и подтвердить")
    @Test
    public void confirmChoosingAllEmployeeForAdding() throws Exception {
        var  project = new PageProject();
        project.createProject();
        project.addParticipantsAction();
        project.fillingAddParticipantToProject();
        project.confirmAddParticipantToProject();
        project.chooseAllEmployeeForAdding();
    }

    @DisplayName ("Отменить удаление участника")
    @Test
    public void cancelDeletingParticipant() throws Exception {
        var  project = new PageProject();
        project.createProject();
        project.addParticipantsAction();
        project.cancelDeleteParticipant();
    }

    @DisplayName ("Подтвердить удаление участника")
    @Test
    public void confirmDeletingParticipant() throws Exception {
        var  project = new PageProject();
        project.createProject();
        project.addParticipantsAction();
        project.confirmDeleteParticipant();
    }*/

}
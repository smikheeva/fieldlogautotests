package mrs.Tests;

import lombok.var;
import mrs.BaseTest;
import mrs.Pages.PageValidation;
import mrs.Pages.PageWorkProgress;
import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ValidationTest extends BaseTest {

    @DisplayName("Проверка валидации при создании проекта")
    @Test
    @Order(1)
    public void projectValidation() throws InterruptedException {
        var valid = new PageValidation();
        startTest();
        valid.projectName0Address0();
        valid.projectName200Address0();
        valid.projectName0Address150();
        valid.projectName10Address150();
    }

    @DisplayName("Проверка валидации при создании ведомости")
    @Test
    public void estimateValidation() throws InterruptedException {
        var valid = new PageValidation();
        var work = new PageWorkProgress();
        startTest();
        work.openProgressWork(idProject);
        valid.estimateName0Code0();
        valid.estimateName0Code1();
        valid.estimateName0Code2();
        valid.estimateName0Code50();
        valid.estimateName5Code50();
        valid.estimateName6Code50();
        valid.estimateName200Code50();
    }

    @DisplayName("Проверка валидации при создании работы")
    @Test
    public void workValidation() throws Exception {
        var valid = new PageValidation();
        var work = new PageWorkProgress();
        startTest();
        work.openProgressWork(idProject);
        valid.createEstimate();
        valid.openEstimate();
        valid.checkRequiredFieldsNewWorkVolume();
        valid.checkRequiredFieldsNewWorkNameAndVolume();
        valid.checkRequiredFieldsNewWorkMeasure();
        valid.checkRequiredFieldsNewWorkMeasureAndName();
        valid.checkRequiredFieldsNewWorkMeasureAndVolume();
        valid.checkRequiredFieldsNewWorkAll();
    }
}
